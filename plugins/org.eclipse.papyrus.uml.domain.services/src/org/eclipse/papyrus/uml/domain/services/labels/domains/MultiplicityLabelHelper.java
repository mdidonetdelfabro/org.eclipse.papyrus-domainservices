/*****************************************************************************
 * Copyright (c) 2009, 2023 CEA LIST.
 *
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Yann TANGUY (CEA LIST) yann.tanguy@cea.fr - Initial API and implementation
 *  Nicolas FAUVERGUE (ALL4TEC) nicolas.fauvergue@all4tec.net - Bug 496905
 *  Obeo - Papyrus Web refactoring
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels.domains;

import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.BACK_SLASH;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.CARD_SEP;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.CLOSE_ANGLE_BRACKET;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.EMPTY;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.MANY;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.OPEN_ANGLE_BRACKET;

import org.eclipse.papyrus.uml.domain.services.labels.INamedElementNameProvider;
import org.eclipse.uml2.uml.LiteralInteger;
import org.eclipse.uml2.uml.LiteralString;
import org.eclipse.uml2.uml.LiteralUnlimitedNatural;
import org.eclipse.uml2.uml.MultiplicityElement;
import org.eclipse.uml2.uml.ValueSpecification;

/**
 * Utility class for <code>org.eclipse.uml2.uml.MultiplicityElement</code>.
 */
// CHECKSTYLE:OFF Papyrus org.eclipse.papyrus.uml.tools.utils.MultiplicityElementUtil
public class MultiplicityLabelHelper {
    // CHECKSTYLE:ON

    private ValueSpecificationLabelHelper valueSpecificationHelper;

    /**
     * Constructor.
     *
     * @param namedElementNameProvider
     *                                 provider of element name
     * 
     */
    public MultiplicityLabelHelper(INamedElementNameProvider namedElementNameProvider) {
        this.valueSpecificationHelper = new ValueSpecificationLabelHelper(namedElementNameProvider);
    }

    /**
     * Return the multiplicity of the element "[x..y]".
     *
     * @param element
     *                The multiplicity element to parse.
     * @return the string representing the multiplicity
     */
    public String formatMultiplicity(final MultiplicityElement element) {
        return this.formatMultiplicity(element, false);
    }

    /**
     * Return the multiplicity of the element "[x..y]" (with quote edition for
     * LiteralString).
     *
     * @param element
     *                  The multiplicity element to parse.
     * @param isEdition
     *                  Boolean to determinate if the quote will be added for the
     *                  LiteralString
     * @return the string representing the multiplicity
     */
    private String formatMultiplicity(final MultiplicityElement element, final boolean isEdition) {
        String multiplicityStr = this.formatMultiplicityNoBrackets(element, isEdition);
        if (multiplicityStr == null || multiplicityStr.isEmpty()) {
            return EMPTY;
        }
        StringBuffer buffer = new StringBuffer();
        buffer.append(OPEN_ANGLE_BRACKET);
        buffer.append(multiplicityStr);
        buffer.append(CLOSE_ANGLE_BRACKET);
        return buffer.toString();
    }

    /**
     * Returns the String corresponding to the multiplicity without square brackets
     * (with quote edition for LiteralString).
     *
     * @param element
     *                  The multiplicity element to parse.
     * @param isEdition
     *                  Boolean to determinate if the quote will be added for the
     *                  LiteralString
     * @return the string representing the multiplicity, without square brackets
     */
    // CHECKSTYLE:OFF Papyrus
    // org.eclipse.papyrus.uml.tools.utils.MultiplicityElementUtil.formatMultiplicityNoBrackets(MultiplicityElement)
    public String formatMultiplicityNoBrackets(final MultiplicityElement element, final boolean isEdition) {
        ValueSpecification lowerSpecification = element.getLowerValue();
        ValueSpecification upperSpecification = element.getUpperValue();
        if (lowerSpecification == null && upperSpecification == null) {
            return this.setupMultiplicityAsInteger(element.getLower(), element.getUpper());
        }
        if (lowerSpecification == null && upperSpecification instanceof LiteralUnlimitedNatural) {
            return this.setupMultiplicityAsInteger(element.getLower(),
                    ((LiteralUnlimitedNatural) upperSpecification).unlimitedValue());
        }
        if (lowerSpecification instanceof LiteralInteger && upperSpecification == null) {
            return this.setupMultiplicityAsInteger(((LiteralInteger) lowerSpecification).integerValue(),
                    element.getUpper());
        }
        if (lowerSpecification instanceof LiteralInteger && upperSpecification instanceof LiteralUnlimitedNatural) {
            return this.setupMultiplicityAsInteger(((LiteralInteger) lowerSpecification).integerValue(),
                    ((LiteralUnlimitedNatural) upperSpecification).unlimitedValue());
        }
        return this.setupMultiplicityAsString(element, lowerSpecification, upperSpecification, isEdition);
    }
    // CHECKSTYLE:ON

    // CHECKSTYLE:OFF Papyrus
    // org.eclipse.papyrus.uml.tools.utils.MultiplicityElementUtil.setupMultiplicityAsInteger(int,
    // int)
    private String setupMultiplicityAsInteger(int lower, int upper) {
        // special case for [1] and [*]
        if (lower == upper) {
            return String.valueOf(lower);
        } else if ((lower == 0) && (upper == -1)) {
            return MANY;
        } else if (upper == -1) {
            return lower + CARD_SEP + MANY;
        } else {
            return lower + CARD_SEP + upper;
        }
    }
    // CHECKSTYLE:ON

    // CHECKSTYLE:OFF Papyrus
    // org.eclipse.papyrus.uml.tools.utils.MultiplicityElementUtil.setupMultiplicityAsString(MultiplicityElement,
    // ValueSpecification, ValueSpecification, boolean)
    private String setupMultiplicityAsString(final MultiplicityElement element, final ValueSpecification lower,
            final ValueSpecification upper, final boolean isEdition) {
        String lowerStr = this.getStringSpecificationValue(lower, isEdition);
        if (MANY.equals(lowerStr)) {
            return EMPTY;
        }
        String upperStr = this.getStringSpecificationValue(upper, isEdition);
        if (lowerStr != null && false == lowerStr.isEmpty() && lowerStr.equalsIgnoreCase(upperStr)) {
            return lowerStr;
        }
        StringBuffer result = new StringBuffer();
        result.append(lowerStr == null || lowerStr.isEmpty() ? element.getLower() : lowerStr);
        result.append(CARD_SEP);
        result.append(upperStr == null || upperStr.isEmpty() ? this.getUpper(element) : upperStr);
        return result.toString();
    }
    // CHECKSTYLE:ON

    // CHECKSTYLE:OFF Papyrus
    // org.eclipse.papyrus.uml.tools.utils.MultiplicityElementUtil.getUpper(MultiplicityElement)
    private String getUpper(MultiplicityElement element) {
        return element.getUpper() == -1 ? EMPTY : String.valueOf(element.getUpper());
    }

    // CHECKSTYLE:ON
    /**
     * This allow to get the value specification (with the quote management for the
     * edition mode).
     *
     * @param valueSpecification
     *                           The value specification.
     * @param isEdition
     *                           Boolean to determinate if the quote will be added
     *                           for the LiteralString
     * @return The string representing the value specification
     */
    private String getStringSpecificationValue(final ValueSpecification valueSpecification, final boolean isEdition) {
        String boundStr = this.valueSpecificationHelper.getSpecificationValue(valueSpecification, true);
        if (isEdition && valueSpecification instanceof LiteralString) {
            final StringBuffer buffer = new StringBuffer();
            buffer.append(BACK_SLASH);
            buffer.append(boundStr);
            buffer.append(BACK_SLASH);
            boundStr = buffer.toString();
        }
        return boundStr;
    }

}
