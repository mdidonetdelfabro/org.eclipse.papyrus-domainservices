/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.modify.ElementFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.modify.IFeatureModifier;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Drop behavior provider of a diagram element to a Use Case Diagram Element.
 * 
 * @author Jessy MALLET
 *
 */
public class UseCaseInternalSourceTorepresentationDropBehaviorProvider
        implements IInternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public Status drop(EObject droppedElement, EObject oldContainer, EObject newContainer,
            ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
        return new UseCaseDropOutsideRepresentationBehaviorProviderSwitch(oldContainer, newContainer, crossRef,
                editableChecker).doSwitch(droppedElement);
    }

    static class UseCaseDropOutsideRepresentationBehaviorProviderSwitch extends UMLSwitch<Status> {

        private final EObject oldContainer;

        private final EObject newContainer;

        private final ECrossReferenceAdapter crossRef;

        private final IEditableChecker editableChecker;

        UseCaseDropOutsideRepresentationBehaviorProviderSwitch(EObject oldContainer, EObject newContainer,
                ECrossReferenceAdapter crossRef, IEditableChecker editableChecker) {
            super();
            this.oldContainer = oldContainer;
            this.newContainer = newContainer;
            this.crossRef = crossRef;
            this.editableChecker = editableChecker;
        }

        /**
         * Default Behavior : UML element can be D&D by using the same reference
         * containment.
         * 
         * @see org.eclipse.uml2.uml.util.UMLSwitch#caseElement(org.eclipse.uml2.uml.Element)
         *
         * @param droppedElement
         *                       the element to drop
         * @return OK or Failing status according to the complete D&D.
         */
        @Override
        public Status caseElement(Element droppedElement) {
            Status dropStatus;
            IFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
            if (oldContainer != newContainer) {
                String refName = droppedElement.eContainmentFeature().getName();
                if (oldContainer.eClass().getEStructuralFeature(refName) != null
                        && newContainer.eClass().getEStructuralFeature(refName) != null) {
                    dropStatus = modifier.removeValue(oldContainer, refName, droppedElement);
                    if (State.DONE == dropStatus.getState()) {
                        dropStatus = modifier.addValue(newContainer, refName, droppedElement);
                    }
                    return dropStatus;
                }

            }
            return super.caseElement(droppedElement);
        }

        @Override
        public Status caseUseCase(UseCase droppedUseCase) {
            Status dropStatus;
            IFeatureModifier modifier = new ElementFeatureModifier(crossRef, editableChecker);
            if (oldContainer != newContainer) {
                String refName = droppedUseCase.eContainmentFeature().getName();
                dropStatus = modifier.removeValue(oldContainer, refName, droppedUseCase);
                if (State.DONE == dropStatus.getState()) {
                    if (newContainer instanceof Classifier) {
                        dropStatus = modifier.addValue(newContainer,
                                UMLPackage.eINSTANCE.getClassifier_OwnedUseCase().getName(), droppedUseCase);
                        ((Classifier) newContainer).getUseCases().add(droppedUseCase);
                    } else if (newContainer instanceof Package) {
                        dropStatus = modifier.addValue(newContainer,
                                UMLPackage.eINSTANCE.getPackage_PackagedElement().getName(), droppedUseCase);
                    }
                    if (oldContainer instanceof Classifier) {
                        ((Classifier) oldContainer).getUseCases().remove(droppedUseCase);
                    }
                }
                return dropStatus;
            }
            return super.caseUseCase(droppedUseCase);
        }

    }
}
