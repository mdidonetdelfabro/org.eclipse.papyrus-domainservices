/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services;

import java.util.List;

import org.eclipse.emf.ecore.EObject;

/**
 * Object that is able to makes queries on a representation model.
 *
 * @author Arthur Daussy
 */
public interface IViewQuerier {

    /**
     * Gets the visual container of the given view.
     *
     * @param view
     *             a view
     * @return the visual container, Node, Diagram or <code>null</code>
     */
    Object getVisualParent(Object view);

    /**
     * Gets the children nodes of a given view.
     * 
     * @param view
     *             a view
     * @return a list of children
     */
    List<? extends Object> getChildrenNodes(Object view);

    /**
     * Gets the bordered nodes of a given view.
     * 
     * @param view
     *             a view
     * @return a list of border nodes
     */
    List<? extends Object> getBorderedNodes(Object view);

    /**
     * Gets the list in order of all visual ancestors of the given view. (fist
     * parent then ancestors)
     * 
     * @param view
     *             a view
     * @return a list of ancestors
     */
    List<? extends Object> getVisualAncestorNodes(Object view);

    /**
     * Get the diagram used to display views.
     * 
     * @return the diagram used to display views.
     */
    Object getDiagram();

    /**
     * Gets the semantic element linked to the given view.
     *
     * @param view
     *             a view element (it can be an edge or a node or a diagram)
     * @return a semantic element or <code>null</code>
     */
    EObject getSemanticElement(Object view);

}
