/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.drop.IInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.FunctionBehavior;
import org.eclipse.uml2.uml.InformationItem;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.util.UMLSwitch;

public class CompositeStructureInternalSourceToRepresentationDropChecker
        implements IInternalSourceToRepresentationDropChecker {

    @Override
    public CheckStatus canDragAndDrop(EObject elementToDrop, EObject newSemanticContainer) {
        return new CompositeStructureDropOutsideRepresentationCheckerSwitch(newSemanticContainer)
                .doSwitch(elementToDrop);
    }

    static class CompositeStructureDropOutsideRepresentationCheckerSwitch extends UMLSwitch<CheckStatus> {

        private EObject newSemanticContainer;

        CompositeStructureDropOutsideRepresentationCheckerSwitch(EObject target) {
            super();
            this.newSemanticContainer = target;
        }

        @Override
        public CheckStatus caseActivity(Activity activity) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof StructuredClassifier
                    || this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Activity can only be drag and drop on a StructuredClassifier or Package.");
            }
            return result;
        }

        @Override
        public CheckStatus caseClass(Class clazz) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof StructuredClassifier
                    || this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Class can only be drag and drop on a StructuredClassifier or Package.");
            }
            return result;
        }

        @Override
        public CheckStatus caseCollaboration(Collaboration collaboration) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof StructuredClassifier
                    || this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Class can only be drag and drop on a StructuredClassifier or Package.");
            }
            return result;
        }

        @Override
        public CheckStatus caseCollaborationUse(CollaborationUse collaborationUse) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof StructuredClassifier) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("CollaborationUse can only be drag and drop on a StructuredClassifier.");
            }
            return result;
        }

        @Override
        public CheckStatus caseComment(Comment comment) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof StructuredClassifier
                    || this.newSemanticContainer instanceof Package || this.newSemanticContainer instanceof Property) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus
                        .no("Comment can only be drag and drop on a StructuredClassifier or Package or Property.");
            }
            return result;
        }

        @Override
        public CheckStatus caseConstraint(Constraint constraint) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof StructuredClassifier
                    || this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Constraint can only be drag and drop on a StructuredClassifier or Package.");
            }
            return result;
        }

        @Override
        public CheckStatus caseFunctionBehavior(FunctionBehavior functionBehavior) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Class || this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("FunctionBehavior can only be drag and drop on a Class or Package.");
            }
            return result;
        }

        @Override
        public CheckStatus caseInformationItem(InformationItem informationItem) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Class || this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("InformationItem can only be drag and drop on a Class or Package.");
            }
            return result;
        }

        @Override
        public CheckStatus caseInteraction(Interaction interaction) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Class || this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("Interaction can only be drag and drop on a Class or Package.");
            }
            return result;
        }

        @Override
        public CheckStatus caseOpaqueBehavior(OpaqueBehavior opaqueBehavior) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Class || this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no("OpaqueBehavior can only be drag and drop on a Class or Package.");
            }
            return result;
        }

        @Override
        public CheckStatus caseParameter(Parameter parameter) {
            final CheckStatus result;
            if (this.newSemanticContainer instanceof StructuredClassifier
                    && !(this.newSemanticContainer instanceof Collaboration
                            // Use eClass to check exact type
                            || this.newSemanticContainer.eClass().equals(UMLPackage.eINSTANCE.getClass_()))) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no(
                        "Parameter can only be drag and drop on a non-Collaboration, non-Class StructuredClassifier.");
            }
            return result;
        }

        @Override
        public CheckStatus caseProperty(Property property) {
            final CheckStatus result;
            if (!(this.newSemanticContainer instanceof StructuredClassifier
                    || (this.newSemanticContainer instanceof Property
                            && ((Property) this.newSemanticContainer).getType() != null)
                            && !(this.newSemanticContainer instanceof Port))) {
                result = CheckStatus
                        .no("Property can only be drag and drop on Structured Classifier or typed Property.");
            } else {
                result = CheckStatus.YES;
            }
            return result;
        }

        @Override
        public CheckStatus caseStateMachine(StateMachine stateMachine) {
            // handles both StateMachine and ProtocolStateMachine
            final CheckStatus result;
            if (this.newSemanticContainer instanceof Class //
                    || this.newSemanticContainer instanceof Package) {
                result = CheckStatus.YES;
            } else {
                result = CheckStatus.no(
                        "StateMachine can only be drag and drop on a non-Collaboration StructuredClassifier or a Package or a Property");
            }
            return result;
        }

        @Override
        public CheckStatus defaultCase(EObject object) {
            return CheckStatus.no("DnD is not authorized.");
        }

    }
}
