/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.labels.domains;

import static org.eclipse.papyrus.uml.domain.services.labels.LabelUtils.getNonNullString;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.D_DOTS;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.SPACE;

import org.eclipse.papyrus.uml.domain.services.labels.INamedElementNameProvider;
import org.eclipse.uml2.uml.CollaborationUse;

/**
 * Utility class for <code>org.eclipse.uml2.uml.CollaborationUse</code>.
 */
// CHECKSTYLE:OFF Papyrus org.eclipse.papyrus.uml.tools.utils.CollaborationUseUtil
public class CollaborationUseLabelHelper {

    // CHECKSTYLE:ON
    public static final String UNDEFINED_TYPE_NAME = "<Undefined>"; //$NON-NLS-1$

    /**
     * Provider used to display element name.
     */
    private INamedElementNameProvider namedElementNameProvider;

    /**
     * Helper used to display visibility signs.
     */
    private VisibilityLabelHelper visibilityLabelHelper;

    /**
     * Constructor.
     *
     * @param namedElementNameProvider
     *                                 provider of element name
     * @param visibilityLabelHelper
     *                                 helper to display visibility signs
     */
    public CollaborationUseLabelHelper(INamedElementNameProvider namedElementNameProvider,
            VisibilityLabelHelper visibilityLabelHelper) {
        super();
        this.namedElementNameProvider = namedElementNameProvider;
        this.visibilityLabelHelper = visibilityLabelHelper;
    }

    /**
     * Get the custom label of the collaborationUse.
     *
     * @param collaborationUse
     *                         the collaborationUse with the label to display
     *
     * @return the string corresponding to the label of the collaborationUse
     */
    public String getLabel(CollaborationUse collaborationUse) {
        StringBuffer buffer = new StringBuffer();

        // visibility
        buffer.append(SPACE);
        buffer.append(getNonNullString(visibilityLabelHelper.getVisibilityAsSign(collaborationUse)));

        // name
        buffer.append(SPACE);
        buffer.append(getNonNullString(this.namedElementNameProvider.getName(collaborationUse)));

        // Separator
        buffer.append(D_DOTS + SPACE);

        // type
        if (collaborationUse.getType() != null) {
            buffer.append(this.namedElementNameProvider.getName(collaborationUse.getType()));
        } else {
            buffer.append(UNDEFINED_TYPE_NAME);
        }

        return buffer.toString();
    }

}
