/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.DnDStatus;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Relationship;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Drop behavior provider of a semantic drop (from Model Explorer view) to a
 * UseCase Diagram Element (or the root of the diagram itself).
 *
 * @author <a href="mailto:vincent.blain@obeo.fr">Vincent Blain</a>
 */
public class UseCaseExternalSourceToRepresentationDropBehaviorProvider
        implements IExternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public DnDStatus drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker) {
        return new UseCaseInsideRepresentationBehaviorProviderSwitch(target).doSwitch(droppedElement);
    }

    static class UseCaseInsideRepresentationBehaviorProviderSwitch extends UMLSwitch<DnDStatus> {

        private final EObject newSemanticContainer;

        UseCaseInsideRepresentationBehaviorProviderSwitch(EObject target) {
            super();
            this.newSemanticContainer = target;
        }

        @Override
        public DnDStatus caseActivity(Activity activity) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(activity));
            }
            return super.caseActivity(activity);
        }

        @Override
        public DnDStatus caseActor(Actor actor) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(actor));
            }
            return super.caseActor(actor);
        }

        @Override
        public DnDStatus caseClass(Class clazz) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(clazz));
            }
            return super.caseClass(clazz);
        }

        @Override
        public DnDStatus caseComment(Comment comment) {
            if (this.newSemanticContainer instanceof Package || this.newSemanticContainer instanceof Class) {
                return DnDStatus.createNothingStatus(Set.of(comment));
            }
            return super.caseComment(comment);
        }

        @Override
        public DnDStatus caseComponent(Component component) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(component));
            }
            return super.caseComponent(component);
        }

        @Override
        public DnDStatus caseConstraint(Constraint constraint) {
            if (this.newSemanticContainer instanceof Package || this.newSemanticContainer instanceof Class) {
                return DnDStatus.createNothingStatus(Set.of(constraint));
            }
            return super.caseConstraint(constraint);
        }

        @Override
        public DnDStatus caseInteraction(Interaction interaction) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(interaction));
            }
            return super.caseInteraction(interaction);
        }

        @Override
        public DnDStatus casePackage(Package pack) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(pack));
            }
            return super.casePackage(pack);
        }

        @Override
        public DnDStatus caseRelationship(Relationship relationship) {
            return DnDStatus.createNothingStatus(Set.of(relationship));
        }

        @Override
        public DnDStatus caseStateMachine(StateMachine stateMachine) {
            if (this.newSemanticContainer instanceof Package) {
                return DnDStatus.createNothingStatus(Set.of(stateMachine));
            }
            return super.caseStateMachine(stateMachine);
        }

        @Override
        public DnDStatus caseUseCase(UseCase useCase) {
            if (this.newSemanticContainer instanceof Package || this.newSemanticContainer instanceof Class) {
                return DnDStatus.createNothingStatus(Set.of(useCase));
            }
            return super.caseUseCase(useCase);
        }

        @Override
        public DnDStatus defaultCase(EObject object) {
            return DnDStatus.createFailingStatus("DnD is forbidden.", Collections.emptySet());
        }
    }
}
