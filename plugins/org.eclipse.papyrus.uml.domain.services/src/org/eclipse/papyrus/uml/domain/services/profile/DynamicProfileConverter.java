/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.profile;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.eclipse.emf.common.util.TreeIterator;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.uml2.common.util.UML2Util;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.util.UMLUtil;
import org.eclipse.uml2.uml.util.UMLUtil.Profile2EPackageConverter;
import org.eclipse.uml2.uml.util.UMLUtil.UML2EcoreConverter;

/**
 * This class add a new dynamic profile EPackage in the given Profile.<br/>
 * 
 * Inspired from
 * {@link org.eclipse.papyrus.uml.diagram.profile.custom.commands.DefineProfileCommand}
 * 
 * @author lfasani
 */
public class DynamicProfileConverter {

    public boolean generateEPackageInProfile(Profile rootProfile, boolean saveConstraint,
            ProfileDefinition profileDefinition) {
        ProfileRedefinitionHelper.removeUndefinedVersion(rootProfile);

        List<EPackage> profileDefinitions = defineProfiles(rootProfile, saveConstraint);
        if (profileDefinitions.size() > 0) {
            ProfileRedefinitionHelper.redefineProfile(rootProfile, profileDefinition);
            ProfileRedefinitionHelper.cleanProfile(rootProfile);
            return true;
        }

        return false;
    }

    /**
     * Define this package if it is a profile and its sub-profiles.
     *
     * @param thePackage
     *                   the package to define (if it is a profile)
     */
    public static List<EPackage> defineProfiles(Package thePackage, boolean saveConstraintInDef) {
        Map<String, String> options = new HashMap<>();

        options.put(UML2EcoreConverter.OPTION__ECORE_TAGGED_VALUES, UMLUtil.OPTION__PROCESS);
        options.put(UML2EcoreConverter.OPTION__DERIVED_FEATURES, UMLUtil.OPTION__REPORT);
        options.put(UML2EcoreConverter.OPTION__DUPLICATE_FEATURE_INHERITANCE, UMLUtil.OPTION__PROCESS);
        options.put(UML2EcoreConverter.OPTION__DUPLICATE_FEATURES, UMLUtil.OPTION__PROCESS);
        options.put(UML2EcoreConverter.OPTION__DUPLICATE_OPERATIONS, UMLUtil.OPTION__REPORT);
        options.put(UML2EcoreConverter.OPTION__DUPLICATE_OPERATION_INHERITANCE, UMLUtil.OPTION__REPORT);
        options.put(UML2EcoreConverter.OPTION__REDEFINING_OPERATIONS, UMLUtil.OPTION__REPORT);
        options.put(UML2EcoreConverter.OPTION__REDEFINING_PROPERTIES, UMLUtil.OPTION__REPORT);
        options.put(UML2EcoreConverter.OPTION__SUBSETTING_PROPERTIES, UMLUtil.OPTION__REPORT);
        options.put(UML2EcoreConverter.OPTION__UNION_PROPERTIES, UMLUtil.OPTION__PROCESS);
        options.put(UML2EcoreConverter.OPTION__SUPER_CLASS_ORDER, UMLUtil.OPTION__REPORT);
        options.put(UML2EcoreConverter.OPTION__ANNOTATION_DETAILS, UMLUtil.OPTION__REPORT);

        // Generate constraints for the validation
        String handleConstraints = UMLUtil.OPTION__PROCESS;
        if (!saveConstraintInDef) {
            handleConstraints = UMLUtil.OPTION__IGNORE;
        }
        options.put(UML2EcoreConverter.OPTION__INVARIANT_CONSTRAINTS, handleConstraints);
        options.put(UML2EcoreConverter.OPTION__VALIDATION_DELEGATES, handleConstraints);
        options.put(UML2EcoreConverter.OPTION__INVOCATION_DELEGATES, handleConstraints);
        options.put(UML2EcoreConverter.OPTION__OPERATION_BODIES, handleConstraints);

        // Assure that "right" (consistent with xtext editor) OCL delegate is used , see
        // bug 512428
        options.put(UML2EcoreConverter.OPTION__OCL_DELEGATE_URI, "http://www.eclipse.org/emf/2002/Ecore/OCL/Pivot"); //$NON-NLS-1$

        options.put(UML2EcoreConverter.OPTION__COMMENTS, UMLUtil.OPTION__IGNORE);
        options.put(Profile2EPackageConverter.OPTION__FOREIGN_DEFINITIONS, UMLUtil.OPTION__PROCESS);

        options.put(UML2EcoreConverter.OPTION__UNTYPED_PROPERTIES, UMLUtil.OPTION__PROCESS); // Closer to the UML
                                                                                             // semantics of untyped
                                                                                             // properties

        List<Profile> toDefine = new ArrayList<>();
        for (TreeIterator<EObject> all = UML2Util.getAllContents(thePackage, true, false); all.hasNext(); /**/) {
            EObject next = all.next();
            if (next instanceof Profile) {
                toDefine.add((Profile) next);
            } else if (!(next instanceof Package)) {
                all.prune();
            }
        }

        if (toDefine.size() > 1) {
            // Sort the profiles in dependency order: used profiles ahead of the profiles
            // that use them. This ensures that
            // Ecore definitions are available for reference in the 4.1.0-style annotations
            // instead of 4.0.0-style
            ProfileUtil.sortProfiles(toDefine);
        }

        List<EPackage> result = new ArrayList<>(toDefine.size());
        for (Profile next : toDefine) {
            result.add(next.define(options, null, null));
        }

        return result;
    }
}
