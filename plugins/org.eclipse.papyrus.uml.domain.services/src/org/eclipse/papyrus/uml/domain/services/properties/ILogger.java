/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.properties;

import org.eclipse.emf.ecore.EObject;

/**
 * Interface used to expose Log API used in properties view services.
 * 
 * @author <a href="mailto:jerome.gout@obeosoft.com">Jerome Gout</a>
 *
 */
public interface ILogger {

    /**
     * 
     * Log message levels. These levels are used to qualify the nature of log
     * messages.
     *
     * @author <a href="mailto:jerome.gout@obeosoft.fr">Jerome Gout</a>
     */
    enum ILogLevel {
        DEBUG, INFO, WARNING, ERROR,
    }

    /**
     * Implementers can handle the given message associated with the logging level
     * in order to inform users.
     * 
     * @param message
     *                The message to display.
     * @param level
     *                The level see {@link ILogger}
     */
    void log(String message, ILogLevel level);

    /**
     * Returns the label of the given object to use in log messages.
     * 
     * @param object
     *               The object which we want the label to display in log messages.
     * @return The string that represents the given object in log messages.
     */
    String getLabelForLog(EObject object);

}
