/*******************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.properties;

import java.text.MessageFormat;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.properties.ILogger.ILogLevel;
import org.eclipse.uml2.uml.LiteralUnlimitedNatural;
import org.eclipse.uml2.uml.MultiplicityElement;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UMLPackage;
import org.eclipse.uml2.uml.ValueSpecification;

/**
 * This service class includes all services used for {@link MultiplicityElement}
 * UML model elements.
 * 
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class PropertiesMultiplicityServices {

	/**
	 * The name of the "lowerValue" feature for a MultiplicityElement.
	 */
	private static final String LOWER_VALUE_FEATURE_NAME = UMLPackage.eINSTANCE.getMultiplicityElement_LowerValue().getName();

	/**
	 * The name of the "upperValue" feature for a MultiplicityElement.
	 */
	private static final String UPPER_VALUE_FEATURE_NAME = UMLPackage.eINSTANCE.getMultiplicityElement_UpperValue().getName();

	/**
	 * The name of the "LiteralUnlimitedNatural" type.
	 */
	private static final String LITERAL_UNLIMITED_NATURAL_ECLASSIFIER_NAME = UMLPackage.eINSTANCE.getLiteralUnlimitedNatural().getName();

	/**
	 * The name of the "LiteralInteger" type.
	 */
	private static final String LITERAL_INTEGER_ECLASSIFIER_NAME = UMLPackage.eINSTANCE.getLiteralInteger().getName();

	/**
	 * The name of the "value" feature for a ValueSpecification.
	 */
	private static final String VALUE = "value"; //$NON-NLS-1$

	/**
	 * The service used to set values and create elements.
	 */
    private final PropertiesCrudServices propertiesSetterServices;

    private ILogger logger;

    public PropertiesMultiplicityServices(ILogger logger, IEditableChecker checker) {
        super();
        this.logger = logger;
        this.propertiesSetterServices = new PropertiesCrudServices(logger, checker);
    }

    /**
     * Gets the String value for a MultiplicityElement. If set, the value returned
     * may be one of the valid format, for example: "1", "0..12", "1..*". Otherwise,
     * an empty string is returned.
     * 
     * @param target
     *               the EObject that should be a MultiplicityElement
     * @return the string value of the multiplicity
     */
	public String getMultiplicity(EObject target) {
		String multiplicityStringValue = PropertiesUMLServices.EMPTY;
		if (target instanceof MultiplicityElement) {
			MultiplicityElement multiplicityElement = (MultiplicityElement) target;
			int lower = multiplicityElement.lowerBound();
			int upper = multiplicityElement.upperBound();
			multiplicityStringValue = MultiplicityParser.getMultiplicity(lower, upper);
		}
		return multiplicityStringValue;

	}

	/**
	 * Sets the multiplicity input string to the MultiplicityElement. If the input string is a valid multiplicity,
	 * lower and upper values are set by creating a LiteralInteger with the specified integer value for lower bound,
	 * and a LiteralUnlimitedNatural with the specified value for the upper bound. The upper bound may be a wildcard "*"
	 * or an Integer. Nothing is done otherwise.
	 * 
	 * @param target
	 *            the MultiplicityElement to change the multiplicity value
	 * @param valueToSet
	 *            the input string used to set lower and upper bounds of multiplicity.
	 *            Example of valid formats: "1", "0..12", "1..*"
	 * @return <code>true</code> if lower and upper values has been updated; <code>false</code> otherwise
	 */
    public boolean setMultiplicity(MultiplicityElement target, String valueToSet) {
        boolean isSet = false;
        if (checkMultiplicityValue(valueToSet)) {
            propertiesSetterServices.delete(target.getLowerValue(), target, LOWER_VALUE_FEATURE_NAME);
            propertiesSetterServices.delete(target.getUpperValue(), target, UPPER_VALUE_FEATURE_NAME);
            int[] bounds = MultiplicityParser.getBounds(valueToSet);
            EObject lowerValue = propertiesSetterServices.create(target, LITERAL_INTEGER_ECLASSIFIER_NAME,
                    LOWER_VALUE_FEATURE_NAME);
            EObject upperValue = propertiesSetterServices.create(target, LITERAL_UNLIMITED_NATURAL_ECLASSIFIER_NAME,
                    UPPER_VALUE_FEATURE_NAME);
            boolean lowerSet = propertiesSetterServices.set(lowerValue, VALUE, Integer.valueOf(bounds[0]));
            boolean upperSet = propertiesSetterServices.set(upperValue, VALUE, Integer.valueOf(bounds[1]));
            isSet = lowerSet && upperSet;
        } else {
            logger.log(MessageFormat.format("''{0}'' is not a valid multiplicity value", valueToSet), ILogLevel.ERROR);
            // Nothing should be done here, we set a value and revert, to force the refresh
            // of the text field.
            ValueSpecification upperValue = target.getUpperValue();
            if (upperValue != null) {
                EStructuralFeature structuralFeature = upperValue.eClass().getEStructuralFeature(VALUE);
                if (structuralFeature != null) {
                    int oldUpper = target.upperBound();
                    upperValue.eSet(structuralFeature, Integer.valueOf(0));
                    upperValue.eSet(structuralFeature, Integer.valueOf(oldUpper));
                }
            } else {
                EClass lUNClass = UMLPackage.eINSTANCE.getLiteralUnlimitedNatural();
                LiteralUnlimitedNatural literalUnlimitedNatural = (LiteralUnlimitedNatural) UMLFactory.eINSTANCE
                        .create(lUNClass);
                target.setUpperValue(literalUnlimitedNatural);
                target.setUpperValue(null);
            }
        }
        return isSet;
	}

    private boolean checkMultiplicityValue(String newValue) {
        boolean result;
        if (newValue instanceof String) {
            result = MultiplicityParser.isValidMultiplicity(newValue);
        } else {
            result = false;
        }
        return result;
    }
}
