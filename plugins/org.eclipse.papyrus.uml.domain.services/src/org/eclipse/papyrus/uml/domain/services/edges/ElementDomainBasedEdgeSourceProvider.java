/*****************************************************************************
 * Copyright (c) 2022, 2024 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.edges;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.internal.helpers.OccurrenceSpecificationHelper;
import org.eclipse.uml2.uml.ActivityEdge;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageEnd;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.OccurrenceSpecification;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Type;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Computes the semantic source of a domain based edge.
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 */
public class ElementDomainBasedEdgeSourceProvider implements IDomainBasedEdgeSourceProvider {

    @Override
    public EObject getSource(EObject semanticElement) {
        return new EdgeProviderSwitch().doSwitch(semanticElement);
    }

    private static class EdgeProviderSwitch extends UMLSwitch<EObject> {

        @Override
        public EObject caseActivityEdge(ActivityEdge activityEdge) {
            return activityEdge.getSource();
        }

        @Override
        public EObject caseAssociation(Association object) {
            // Source of association is the type of the target property
            Type type = null;
            if (object.getMemberEnds().size() > 1) {
                type = object.getMemberEnds().get(1).getType();
            }
            return type;
        }

        @Override
        public EObject caseComponentRealization(ComponentRealization componentRealization) {
            if (componentRealization.getRealizingClassifiers().isEmpty()) {
                return null;
            } else {
                // Only binary ComponentRealization are supported.
                return componentRealization.getRealizingClassifiers().get(0);
            }
        }

        @Override
        public EObject caseConnector(Connector connector) {
            // Simplified version for the moment
            return connector.getEnds().stream()//
                    .filter(end -> end.getRole() != null)//
                    .map(end -> end.getRole())//
                    .findFirst().orElse(null);
        }

        @Override
        public EObject caseDependency(Dependency dependency) {
            EList<NamedElement> clients = dependency.getClients();
            if (clients.isEmpty()) {
                return null;
            } else {
                return clients.get(0);
            }
        }

        @Override
        public EObject caseDeployment(Deployment deployment) {
            if (deployment.getDeployedArtifacts().isEmpty()) {
                return null;
            } else {
                // Only binary Deployment are supported.
                return deployment.getDeployedArtifacts().get(0);
            }
        }

        @Override
        public EObject caseElementImport(ElementImport elementImport) {
            return elementImport.getImportingNamespace();
        }

        @Override
        public EObject caseExtend(Extend extend) {
            return extend.getExtension();
        }

        @Override
        public EObject caseExtension(Extension extension) {
            return extension.getStereotype();
        }

        @Override
        public EObject caseGeneralization(Generalization object) {
            return object.getSpecific();
        }

        @Override
        public EObject caseInclude(Include object) {
            return object.getIncludingCase();
        }

        @Override
        public EObject caseInformationFlow(InformationFlow informationFlow) {
            return informationFlow.getInformationSources().stream().findFirst().orElse(null);
        }

        @Override
        public EObject caseInterfaceRealization(InterfaceRealization interfaceRealization) {
            return interfaceRealization.getImplementingClassifier();
        }

        @Override
        public EObject caseMessage(Message message) {
            MessageEnd sendEvent = message.getSendEvent();
            if (sendEvent instanceof OccurrenceSpecification) {
                return OccurrenceSpecificationHelper.findEnclosingElement((OccurrenceSpecification) sendEvent);
            }
            return null;
        }

        @Override
        public EObject casePackageImport(PackageImport object) {
            return object.getImportingNamespace();
        }

        @Override
        public EObject casePackageMerge(PackageMerge object) {
            return object.getReceivingPackage();
        }

        @Override
        public EObject caseSubstitution(Substitution object) {
            return object.getSubstitutingClassifier();
        }

        @Override
        public EObject caseTransition(Transition transition) {
            return transition.getSource();
        }

    }
}
