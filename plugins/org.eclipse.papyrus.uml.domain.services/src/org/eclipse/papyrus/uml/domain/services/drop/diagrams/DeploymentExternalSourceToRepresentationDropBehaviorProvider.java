/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop.diagrams;

import java.util.Collections;
import java.util.Set;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.util.ECrossReferenceAdapter;
import org.eclipse.papyrus.uml.domain.services.IEditableChecker;
import org.eclipse.papyrus.uml.domain.services.drop.DnDStatus;
import org.eclipse.papyrus.uml.domain.services.drop.IExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.CommunicationPath;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.DeploymentSpecification;
import org.eclipse.uml2.uml.Device;
import org.eclipse.uml2.uml.ExecutionEnvironment;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.util.UMLSwitch;

/**
 * Drop behavior provider of a semantic drop (from Model Explorer view) to a
 * Deployment Diagram Element (or the root of the diagram itself).
 * 
 * @author <a href="mailto:florian.barbin@obeo.fr">Florian Barbin</a>
 *
 */
public class DeploymentExternalSourceToRepresentationDropBehaviorProvider
implements IExternalSourceToRepresentationDropBehaviorProvider {

    @Override
    public DnDStatus drop(EObject droppedElement, EObject target, ECrossReferenceAdapter crossRef,
            IEditableChecker editableChecker) {
        return new DeploymentDropInsideRepresentationBehaviorProviderSwitch().doSwitch(droppedElement);
    }

    static class DeploymentDropInsideRepresentationBehaviorProviderSwitch extends UMLSwitch<DnDStatus> {

        @Override
        public DnDStatus caseArtifact(Artifact artifact) {
            return DnDStatus.createNothingStatus(Set.of(artifact));
        }

        @Override
        public DnDStatus caseComment(Comment comment) {
            return DnDStatus.createNothingStatus(Set.of(comment));
        }

        @Override
        public DnDStatus caseCommunicationPath(CommunicationPath communicationPath) {
            return DnDStatus.createNothingStatus(Set.of(communicationPath));
        }

        @Override
        public DnDStatus caseConstraint(Constraint constraint) {
            return DnDStatus.createNothingStatus(Set.of(constraint));
        }

        @Override
        public DnDStatus caseDependency(Dependency dependency) {
            return DnDStatus.createNothingStatus(Set.of(dependency));
        }

        @Override
        public DnDStatus caseDeployment(Deployment deployment) {
            return DnDStatus.createNothingStatus(Set.of(deployment));
        }

        @Override
        public DnDStatus caseDeploymentSpecification(DeploymentSpecification deploymentSpecification) {
            return DnDStatus.createNothingStatus(Set.of(deploymentSpecification));
        }

        @Override
        public DnDStatus caseDevice(Device device) {
            return DnDStatus.createNothingStatus(Set.of(device));
        }

        @Override
        public DnDStatus caseExecutionEnvironment(ExecutionEnvironment executionEnvironment) {
            return DnDStatus.createNothingStatus(Set.of(executionEnvironment));
        }

        @Override
        public DnDStatus caseGeneralization(Generalization generalization) {
            return DnDStatus.createNothingStatus(Set.of(generalization));
        }

        @Override
        public DnDStatus caseManifestation(Manifestation manifestation) {
            return DnDStatus.createNothingStatus(Set.of(manifestation));
        }

        @Override
        public DnDStatus caseModel(Model model) {
            return DnDStatus.createNothingStatus(Set.of(model));
        }

        @Override
        public DnDStatus caseNode(Node node) {
            return DnDStatus.createNothingStatus(Set.of(node));
        }

        @Override
        public DnDStatus casePackage(Package pkg) {
            return DnDStatus.createNothingStatus(Set.of(pkg));
        }

        @Override
        public DnDStatus defaultCase(EObject obj) {
            return DnDStatus.createFailingStatus("DnD is forbidden.", Collections.emptySet());
        }

    }

}
