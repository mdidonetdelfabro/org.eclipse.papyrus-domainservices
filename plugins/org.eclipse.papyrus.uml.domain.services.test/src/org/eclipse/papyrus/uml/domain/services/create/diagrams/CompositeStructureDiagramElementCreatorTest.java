/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, OBEO
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create.diagrams;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.emf.ecore.EObject;
import org.eclipse.papyrus.uml.domain.services.create.CreationStatus;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.DataType;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.UMLPackage;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CompositeStructureDiagramElementCreatorTest extends AbstractUMLTest {

    private UMLPackage pack = UMLPackage.eINSTANCE;

    private CompositeStructureDiagramElementCreator creator;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        creator = CompositeStructureDiagramElementCreator.buildDefault(getCrossRef(), getEditableChecker());
    }

    @Test
    public void createPortOnUntypedProperty() {

        Property property = create(Property.class);
        CreationStatus status = this.creator.create(property, this.pack.getPort().getName(),
                this.pack.getStructuredClassifier_OwnedAttribute().getName());
        // No type => no port creation on the type
        assertEquals(State.FAILED, status.getState());
        assertNull(status.getElement());
    }

    @Test
    public void createPortOnSimpleTypeProperty() {
        Property property = create(Property.class);
        DataType dt = create(DataType.class);
        property.setType(dt);

        CreationStatus status = this.creator.create(property, this.pack.getPort().getName(),
                this.pack.getStructuredClassifier_OwnedAttribute().getName());
        // The port can only be created if the type of the property is a nested
        // classifier
        assertEquals(State.FAILED, status.getState());
        assertNull(status.getElement());
    }

    @Test
    public void createPortOnProperty() {
        Property property = create(Property.class);
        org.eclipse.uml2.uml.Class clazz = create(org.eclipse.uml2.uml.Class.class);
        property.setType(clazz);

        CreationStatus status = this.creator.create(property, this.pack.getPort().getName(),
                this.pack.getStructuredClassifier_OwnedAttribute().getName());
        // The port can only be created if the type of the property is a nested
        // classifier
        assertEquals(State.DONE, status.getState());
        EObject newElement = status.getElement();
        assertTrue(newElement instanceof Port);
        assertTrue(clazz.getOwnedAttributes().contains(newElement));
        assertEquals("Port1", ((Port) newElement).getName()); //$NON-NLS-1$
    }

    @Test
    public void createPropertyOnTypeProperty() {
        Property property = create(Property.class);
        org.eclipse.uml2.uml.Class clazz = create(org.eclipse.uml2.uml.Class.class);
        property.setType(clazz);

        CreationStatus status = this.creator.create(property, this.pack.getProperty().getName(),
                this.pack.getStructuredClassifier_OwnedAttribute().getName());
        // The port can only be created if the type of the property is a nested
        // classifier
        assertEquals(State.DONE, status.getState());
        EObject newElement = status.getElement();
        assertTrue(newElement instanceof Property);
        assertTrue(clazz.getOwnedAttributes().contains(newElement));
        assertEquals("Property1", ((Property) newElement).getName()); //$NON-NLS-1$
    }

    @Test
    public void createPropertyOnUntypeProperty() {
        Property property = create(Property.class);
        CreationStatus status = this.creator.create(property, this.pack.getProperty().getName(),
                this.pack.getStructuredClassifier_OwnedAttribute().getName());
        assertEquals(State.FAILED, status.getState());
    }

}
