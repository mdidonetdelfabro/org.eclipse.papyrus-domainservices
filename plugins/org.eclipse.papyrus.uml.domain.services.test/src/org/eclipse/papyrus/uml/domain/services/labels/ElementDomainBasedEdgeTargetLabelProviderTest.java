/*******************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - initial API and implementation
 *******************************************************************************/

package org.eclipse.papyrus.uml.domain.services.labels;

import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.ST_LEFT;
import static org.eclipse.papyrus.uml.domain.services.labels.UMLCharacters.ST_RIGHT;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.DecisionNode;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeTargetLabelProviderTest}.
 * 
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 *
 */
public class ElementDomainBasedEdgeTargetLabelProviderTest extends AbstractUMLTest {

    private static final String DECISION_INPUT_FLOW = "decisionInputFlow";
    private ElementDomainBasedEdgeTargetLabelProvider labelProvider = ElementDomainBasedEdgeTargetLabelProvider
            .buildDefault();

    /**
     * Test source label for {@link Association}.
     */
    @Test
    public void testAssociationTargetLabel() {
        Property source = create(Property.class);
        Property target = create(Property.class);
        target.setName("myProp");
        Association association = create(Association.class);
        association.getMemberEnds().add(target);
        association.getMemberEnds().add(source);

        assertEquals("+ myProp 1", this.labelProvider.getLabel(association, null));

        target.setUpper(-1);
        assertEquals("+ myProp 1..*", this.labelProvider.getLabel(association, null));

        target.setLower(0);
        assertEquals("+ myProp *", this.labelProvider.getLabel(association, null));

        target.setLower(5);
        target.setUpper(6);
        assertEquals("+ myProp 5..6", this.labelProvider.getLabel(association, null));
    }

    /**
     * Test source label for {@link Connector}.
     */
    @Test
    public void testConnectorTargetLabel() {

        Port source = create(Port.class);
        Port target = create(Port.class);

        Connector connector = create(Connector.class);
        ConnectorEnd conEndSource = createIn(ConnectorEnd.class, connector);
        conEndSource.setRole(source);

        ConnectorEnd conEndTarget = createIn(ConnectorEnd.class, connector);
        conEndTarget.setRole(target);

        assertEquals("1", this.labelProvider.getLabel(connector, target));

        conEndTarget.setUpper(-1);
        assertEquals("1..*", this.labelProvider.getLabel(connector, target));

        conEndTarget.setLower(0);
        assertEquals("*", this.labelProvider.getLabel(connector, target));

        conEndTarget.setLower(5);
        conEndTarget.setUpper(6);
        assertEquals("5..6", this.labelProvider.getLabel(connector, target));
    }

    @Test
    public void testObjectFlowTargetLabel() {
        InputPin inputPin = create(InputPin.class);
        DecisionNode decisionNode = create(DecisionNode.class);
        ObjectFlow objectFlow = create(ObjectFlow.class);
        objectFlow.setTarget(inputPin);
        assertEquals("", this.labelProvider.getLabel(objectFlow, inputPin));
        objectFlow.setTarget(decisionNode);
        assertEquals("", this.labelProvider.getLabel(objectFlow, decisionNode));
        decisionNode.setDecisionInputFlow(objectFlow);
        assertEquals(ST_LEFT + DECISION_INPUT_FLOW + ST_RIGHT, this.labelProvider.getLabel(objectFlow, decisionNode));
    }

}
