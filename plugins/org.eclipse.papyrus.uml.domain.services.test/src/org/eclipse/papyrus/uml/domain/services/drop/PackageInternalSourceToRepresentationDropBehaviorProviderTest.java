/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.PackageInternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Package;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for
 * {@link PackageInternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author <a href="mailto:glenn.plouhinec@obeo.fr">Glenn Plouhinec</a>
 */
public class PackageInternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    private PackageInternalSourceToRepresentationDropBehaviorProvider packageInternalSourceToRepresentationDropBehaviorProvider;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.packageInternalSourceToRepresentationDropBehaviorProvider = new PackageInternalSourceToRepresentationDropBehaviorProvider();
    }

    @Test
    public void testCommentDropFromPackageToPackage() {
        Comment commentToDrop = this.create(Comment.class);
        Package pack1 = this.create(Package.class);
        Package pack2 = this.create(Package.class);
        pack1.getOwnedComments().add(commentToDrop);

        Status status = this.packageInternalSourceToRepresentationDropBehaviorProvider.drop(commentToDrop, pack1, pack2,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertTrue(pack1.getOwnedComments().isEmpty());
        assertTrue(pack2.getOwnedComments().contains(commentToDrop));
    }

    @Test
    public void testConstraintDropFromPackageToPackage() {
        Constraint constraintToDrop = this.create(Constraint.class);
        Package pack1 = this.create(Package.class);
        Package pack2 = this.create(Package.class);
        pack1.getOwnedRules().add(constraintToDrop);

        Status status = this.packageInternalSourceToRepresentationDropBehaviorProvider.drop(constraintToDrop, pack1,
                pack2, this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertTrue(pack1.getOwnedRules().isEmpty());
        assertTrue(pack2.getOwnedRules().contains(constraintToDrop));
    }

    @Test
    public void testPackageDropFromPackageToPackage() {
        Package packageToDrop = this.create(Package.class);
        Package pack1 = this.create(Package.class);
        Package pack2 = this.create(Package.class);
        pack1.getPackagedElements().add(packageToDrop);

        Status status = this.packageInternalSourceToRepresentationDropBehaviorProvider.drop(packageToDrop, pack1, pack2,
                this.getCrossRef(), this.getEditableChecker());
        assertEquals(State.DONE, status.getState());
        assertTrue(pack1.getPackagedElements().isEmpty());
        assertTrue(pack2.getPackagedElements().contains(packageToDrop));
    }

}
