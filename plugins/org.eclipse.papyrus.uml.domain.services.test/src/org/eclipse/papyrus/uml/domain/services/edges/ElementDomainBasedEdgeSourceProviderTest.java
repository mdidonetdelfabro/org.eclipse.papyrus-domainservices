/*****************************************************************************
 * Copyright (c) 2022, 2024 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/

package org.eclipse.papyrus.uml.domain.services.edges;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

import java.util.List;

import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.ActionExecutionSpecification;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.ElementImport;
import org.eclipse.uml2.uml.ExecutionOccurrenceSpecification;
import org.eclipse.uml2.uml.ExecutionSpecification;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.ExtensionEnd;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.InterfaceRealization;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.MessageOccurrenceSpecification;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeSourceProviderTest}.
 *
 * @author <a href="mailto:arthur.daussy@obeo.fr">Arthur Daussy</a>
 *
 */
public class ElementDomainBasedEdgeSourceProviderTest extends AbstractUMLTest {

    private ElementDomainBasedEdgeSourceProvider sourceProvider = new ElementDomainBasedEdgeSourceProvider();

    @Test
    public void testAssociation() {
        Association association = this.create(Association.class);
        Actor source = this.create(Actor.class);
        Actor target = this.create(Actor.class);

        new ElementDomainBasedEdgeInitializer().initialize(association, source, target, null, null, null);

        assertEquals(source, this.sourceProvider.getSource(association));

        // if type of target Property is null, none source is found
        association.getMemberEnds().get(1).setType(null);
        assertEquals(null, this.sourceProvider.getSource(association));
    }

    /**
     * Tests the source of a {@link ComponentRealization}.
     */
    @Test
    public void testComponentRealization() {
        ComponentRealization componentRealization = this.create(ComponentRealization.class);
        assertNull(this.sourceProvider.getSource(componentRealization));

        Interface interface1 = this.create(Interface.class);
        componentRealization.getRealizingClassifiers().add(interface1);
        assertEquals(interface1, this.sourceProvider.getSource(componentRealization));

        // ComponentRealization can have multiple realizing classifiers, in this case we
        // return the first.
        Interface interface2 = this.create(Interface.class);
        componentRealization.getRealizingClassifiers().add(interface2);
        assertEquals(interface1, this.sourceProvider.getSource(componentRealization));
    }

    /**
     * Checks the source provider for {@link ControlFlow}.
     */
    @Test
    public void testControlFlow() {
        ControlFlow controlFlow = this.create(ControlFlow.class);
        InputPin inputPin = this.create(InputPin.class);
        OutputPin outputPin = this.create(OutputPin.class);
        controlFlow.setSource(outputPin);
        controlFlow.setTarget(inputPin);
        assertEquals(outputPin, this.sourceProvider.getSource(controlFlow));
    }

    /**
     * Tests source provider on dependency (and some inherited element).
     */
    @Test
    public void testDependency() {

        Dependency dependency = this.create(Dependency.class);
        assertNull(this.sourceProvider.getSource(dependency));

        Class class1 = this.create(org.eclipse.uml2.uml.Class.class);
        dependency.getClients().add(class1);
        assertEquals(class1, this.sourceProvider.getSource(dependency));
    }

    @Test
    public void testDeployment() {
        Deployment deployment = this.create(Deployment.class);
        assertNull(this.sourceProvider.getSource(deployment));

        Artifact artifact1 = this.create(Artifact.class);
        deployment.getDeployedArtifacts().add(artifact1);
        assertEquals(artifact1, this.sourceProvider.getSource(deployment));

        // Deployment can have multiple deployed artifacts, in this case we
        // return the first.
        Artifact artifact2 = this.create(Artifact.class);
        deployment.getDeployedArtifacts().add(artifact2);
        assertEquals(artifact1, this.sourceProvider.getSource(deployment));
    }

    @Test
    public void testElementImport() {
        ElementImport elementImport = this.create(ElementImport.class);
        Profile source = this.create(Profile.class);
        Class target = this.create(Class.class);

        new ElementDomainBasedEdgeInitializer().initialize(elementImport, source, target, null, null, null);
        assertEquals(source, this.sourceProvider.getSource(elementImport));
    }

    @Test
    public void testExtend() {
        Extend extend = this.create(Extend.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);

        new ElementDomainBasedEdgeInitializer().initialize(extend, source, target, null, null, null);

        assertEquals(source, this.sourceProvider.getSource(extend));
    }

    /**
     * Tests source provider for {@link Extension}.
     */
    @Test
    public void testExtension() {
        Extension extension = this.create(Extension.class);
        ExtensionEnd extensionEnd = this.create(ExtensionEnd.class);
        extension.getOwnedEnds().add(extensionEnd);
        Stereotype stereotype = this.create(Stereotype.class);
        extensionEnd.setType(stereotype);
        Property property = this.create(Property.class);
        Class class1 = this.create(org.eclipse.uml2.uml.Class.class);
        property.setType(class1);
        property.setAssociation(extension);
        extension.getMemberEnds().add(property);
        stereotype.getOwnedAttributes().add(property);
        assertEquals(stereotype, this.sourceProvider.getSource(extension));

    }

    /**
     * Tests source provider on Generalization.
     */
    @Test
    public void testGeneralization() {

        Generalization generalization = this.create(Generalization.class);
        assertNull(this.sourceProvider.getSource(generalization));

        Class class1 = this.create(org.eclipse.uml2.uml.Class.class);
        generalization.setSpecific(class1);
        assertEquals(class1, this.sourceProvider.getSource(generalization));
    }

    @Test
    public void testInclude() {
        Include include = this.create(Include.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);

        new ElementDomainBasedEdgeInitializer().initialize(include, source, target, null, null, null);

        assertEquals(source, this.sourceProvider.getSource(include));
    }

    /**
     * Tests source provider on {@link InformationFlow}.
     */
    @Test
    public void testInformationFlow() {

        InformationFlow informationFlow = this.create(InformationFlow.class);
        assertNull(this.sourceProvider.getSource(informationFlow));

        Class class1 = this.create(org.eclipse.uml2.uml.Class.class);
        informationFlow.getInformationSources().add(class1);
        assertEquals(class1, this.sourceProvider.getSource(informationFlow));
    }

    /**
     * Tests the source of an InterfaceRealization.
     */
    @Test
    public void testInterfaceRealization() {
        InterfaceRealization interfaceRealization = this.create(InterfaceRealization.class);
        assertNull(this.sourceProvider.getSource(interfaceRealization));

        Class class1 = this.create(org.eclipse.uml2.uml.Class.class);
        interfaceRealization.setImplementingClassifier(class1);
        assertEquals(class1, this.sourceProvider.getSource(interfaceRealization));
    }

    /**
     * Tests source provider on {link Manifestation}.
     */
    @Test
    public void testManifestation() {

        Manifestation manifestation = this.create(Manifestation.class);
        assertNull(this.sourceProvider.getSource(manifestation));

        Class class1 = this.create(org.eclipse.uml2.uml.Class.class);
        manifestation.getClients().add(class1);
        assertEquals(class1, this.sourceProvider.getSource(manifestation));
    }

    /**
     * Tests source provider on {@link Message} when {@link Message#getSendEvent()}
     * is after an {@link ExecutionSpecification}.
     * <p>
     * The expected result is the {@link Lifeline} covered by
     * {@link Message#getSendEvent()}.
     */
    @Test
    public void testMessageExecutionSpecificationAfterSendEvent() {
        Interaction interaction = this.create(Interaction.class);
        Message message = this.create(Message.class);
        MessageOccurrenceSpecification messageSendEvent = this.create(MessageOccurrenceSpecification.class);
        message.setSendEvent(messageSendEvent);
        Lifeline sourceLifeline = this.create(Lifeline.class);
        messageSendEvent.setMessage(message);
        messageSendEvent.getCovereds().add(sourceLifeline);

        ExecutionSpecification executionSpecification = this.create(ActionExecutionSpecification.class);
        executionSpecification.getCovereds().add(sourceLifeline);
        ExecutionOccurrenceSpecification executionSpecificationStart = this
                .create(ExecutionOccurrenceSpecification.class);
        executionSpecificationStart.getCovereds().add(sourceLifeline);
        executionSpecification.setStart(executionSpecificationStart);
        executionSpecificationStart.setExecution(executionSpecification);
        ExecutionOccurrenceSpecification executionSpecificationFinish = this
                .create(ExecutionOccurrenceSpecification.class);
        executionSpecificationFinish.getCovereds().add(sourceLifeline);
        executionSpecification.setFinish(executionSpecificationFinish);
        executionSpecificationFinish.setExecution(executionSpecification);
        interaction.getFragments().addAll(List.of(messageSendEvent, executionSpecificationStart, executionSpecification,
                executionSpecificationFinish));

        Lifeline target = this.create(Lifeline.class);

        new ElementDomainBasedEdgeInitializer().initialize(message, executionSpecification, target, null, null, null);

        assertEquals(sourceLifeline, this.sourceProvider.getSource(message));
    }

    /**
     * Tests source provider on {@link Message} when {@link Message#getSendEvent()}
     * is before an {@link ExecutionSpecification}.
     * <p>
     * The expected result is the {@link Lifeline} covered by
     * {@link Message#getSendEvent()}.
     */
    @Test
    public void testMessageExecutionSpecificationBeforeSendEvent() {
        Interaction interaction = this.create(Interaction.class);
        Message message = this.create(Message.class);
        MessageOccurrenceSpecification messageSendEvent = this.create(MessageOccurrenceSpecification.class);
        message.setSendEvent(messageSendEvent);
        Lifeline sourceLifeline = this.create(Lifeline.class);
        messageSendEvent.setMessage(message);
        messageSendEvent.getCovereds().add(sourceLifeline);

        ExecutionSpecification executionSpecification = this.create(ActionExecutionSpecification.class);
        executionSpecification.getCovereds().add(sourceLifeline);
        ExecutionOccurrenceSpecification executionSpecificationStart = this
                .create(ExecutionOccurrenceSpecification.class);
        executionSpecificationStart.getCovereds().add(sourceLifeline);
        executionSpecification.setStart(executionSpecificationStart);
        executionSpecificationStart.setExecution(executionSpecification);
        ExecutionOccurrenceSpecification executionSpecificationFinish = this
                .create(ExecutionOccurrenceSpecification.class);
        executionSpecificationFinish.getCovereds().add(sourceLifeline);
        executionSpecification.setFinish(executionSpecificationFinish);
        executionSpecificationFinish.setExecution(executionSpecification);
        interaction.getFragments().addAll(List.of(executionSpecificationStart, executionSpecification,
                executionSpecificationFinish, messageSendEvent));

        Lifeline target = this.create(Lifeline.class);

        new ElementDomainBasedEdgeInitializer().initialize(message, executionSpecification, target, null, null, null);

        assertEquals(sourceLifeline, this.sourceProvider.getSource(message));
    }

    /**
     * Tests source provider on {@link Message} when {@link Message#getSendEvent()}
     * is enclosed in an {@link ExecutionSpecification}.
     * <p>
     * The expected result is the {@link ExecutionSpecification} enclosing the
     * {@link Message#getSendEvent()}.
     */
    @Test
    public void testMessageExecutionSpecificationEnclosingSendEvent() {
        Interaction interaction = this.create(Interaction.class);
        Message message = this.create(Message.class);
        MessageOccurrenceSpecification messageSendEvent = this.create(MessageOccurrenceSpecification.class);
        message.setSendEvent(messageSendEvent);
        Lifeline sourceLifeline = this.create(Lifeline.class);
        messageSendEvent.setMessage(message);
        messageSendEvent.getCovereds().add(sourceLifeline);

        ExecutionSpecification executionSpecification = this.create(ActionExecutionSpecification.class);
        executionSpecification.getCovereds().add(sourceLifeline);
        ExecutionOccurrenceSpecification executionSpecificationStart = this
                .create(ExecutionOccurrenceSpecification.class);
        executionSpecificationStart.getCovereds().add(sourceLifeline);
        executionSpecification.setStart(executionSpecificationStart);
        executionSpecificationStart.setExecution(executionSpecification);
        ExecutionOccurrenceSpecification executionSpecificationFinish = this
                .create(ExecutionOccurrenceSpecification.class);
        executionSpecificationFinish.getCovereds().add(sourceLifeline);
        executionSpecification.setFinish(executionSpecificationFinish);
        executionSpecificationFinish.setExecution(executionSpecification);
        interaction.getFragments().addAll(List.of(executionSpecificationStart, executionSpecification, messageSendEvent,
                executionSpecificationFinish));

        Lifeline target = this.create(Lifeline.class);

        new ElementDomainBasedEdgeInitializer().initialize(message, executionSpecification, target, null, null, null);

        assertEquals(executionSpecification, this.sourceProvider.getSource(message));
    }

    /**
     * Tests source provider on {@link Message} when {@link Message#getSendEvent()}
     * is the only element of its {@link Lifeline}.
     * <p>
     * The expected result is the {@link Lifeline} covered by
     * {@link Message#getSendEvent()}.
     */
    @Test
    public void testMessageLifelineSource() {
        Interaction interaction = this.create(Interaction.class);
        Message message = this.create(Message.class);
        MessageOccurrenceSpecification messageSendEvent = this.create(MessageOccurrenceSpecification.class);
        message.setSendEvent(messageSendEvent);
        messageSendEvent.setMessage(message);
        Lifeline source = this.create(Lifeline.class);
        messageSendEvent.getCovereds().add(source);
        Lifeline target = this.create(Lifeline.class);
        interaction.getFragments().add(messageSendEvent);

        new ElementDomainBasedEdgeInitializer().initialize(message, source, target, null, null, null);

        assertEquals(source, this.sourceProvider.getSource(message));
    }

    /**
     * Tests source provider on {@link Message} when {@link Message#getSendEvent()}
     * is {@code null}.
     * <p>
     * The expected result is {@code null}.
     */
    @Test
    public void testMessageNullSendEvent() {
        Message message = this.create(Message.class);
        Lifeline source = this.create(Lifeline.class);
        Lifeline target = this.create(Lifeline.class);

        new ElementDomainBasedEdgeInitializer().initialize(message, source, target, null, null, null);

        assertEquals(null, this.sourceProvider.getSource(message));
    }

    /**
     * Checks the source provider for {@link ObjectFlow}.
     */
    @Test
    public void testObjectFlow() {
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        InputPin inputPin = this.create(InputPin.class);
        OutputPin outputPin = this.create(OutputPin.class);
        objectFlow.setSource(outputPin);
        objectFlow.setTarget(inputPin);
        assertEquals(outputPin, this.sourceProvider.getSource(objectFlow));
    }

    /**
     * Tests source provider on {link PackageImport}.
     */
    @Test
    public void testPackageImport() {
        PackageImport element = this.create(PackageImport.class);
        assertNull(this.sourceProvider.getSource(element));

        Package src = this.create(org.eclipse.uml2.uml.Package.class);
        element.setImportingNamespace(src);
        assertEquals(src, this.sourceProvider.getSource(element));
    }

    /**
     * Tests source provider on {link PackageMerge}.
     */
    @Test
    public void testPackageMerge() {
        PackageMerge element = this.create(PackageMerge.class);
        assertNull(this.sourceProvider.getSource(element));

        Package src = this.create(org.eclipse.uml2.uml.Package.class);
        element.setReceivingPackage(src);
        assertEquals(src, this.sourceProvider.getSource(element));
    }

    /**
     * Tests source provider on {link Substitution}.
     */
    @Test
    public void testSubstitution() {

        Substitution substitution = this.create(Substitution.class);
        assertNull(this.sourceProvider.getSource(substitution));

        Class class1 = this.create(org.eclipse.uml2.uml.Class.class);
        substitution.setSubstitutingClassifier(class1);
        assertEquals(class1, this.sourceProvider.getSource(substitution));
    }

    @Test
    public void testTransition() {
        Pseudostate source = this.create(Pseudostate.class);
        Pseudostate target = this.create(Pseudostate.class);

        Transition transition = this.create(Transition.class);
        transition.setSource(source);
        transition.setTarget(target);

        assertEquals(source, this.sourceProvider.getSource(transition));
    }

}
