/*****************************************************************************
 * Copyright (c) 2022 CEA LIST, Obeo
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  OBEO - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.UseCaseExternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link UseCaseExternalSourceToRepresentationDropChecker}.
 *
 * @author Jessy Mallet
 *
 */
public class UseCaseExternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private UseCaseExternalSourceToRepresentationDropChecker useCaseExternalSourceToRepresentationDropChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        this.useCaseExternalSourceToRepresentationDropChecker = new UseCaseExternalSourceToRepresentationDropChecker();
    }

    /**
     * Test dropping a {@link Activity}.
     */
    @Test
    public void testActivityDrop() {
        Package pack = this.create(Package.class);
        Activity activity = this.create(Activity.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = this.create(Class.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(activity, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = this.create(Actor.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(activity, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Actor}.
     */
    @Test
    public void testActorDrop() {
        Actor actor = this.create(Actor.class);
        Package pack = this.create(Package.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(actor,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz = this.create(Class.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(actor, clazz);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping an element that cannot be dropped on the UseCase diagram.
     */
    @Test
    public void testArtifactDrop() {
        Package pack = this.create(Package.class);
        Artifact artifact = this.create(Artifact.class);

        // not authorized D&D
        CheckStatus canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker
                .canDragAndDrop(artifact, pack);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class}.
     */
    @Test
    public void testClassDrop() {
        Package pack = this.create(Package.class);
        Class clazz = this.create(Class.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(clazz,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = this.create(Class.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(clazz, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = this.create(Actor.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(clazz, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment}.
     */
    @Test
    public void testCommentDrop() {
        Package pack = this.create(Package.class);
        Comment comment = this.create(Comment.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(comment,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Subject
        Class clazz = this.create(Class.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(comment, clazz);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = this.create(Actor.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(comment, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Component}.
     */
    @Test
    public void testComponentDrop() {
        Package pack = this.create(Package.class);
        Component component = this.create(Component.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker
                .canDragAndDrop(component, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = this.create(Class.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(component, clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = this.create(Actor.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(component, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint}.
     */
    @Test
    public void testConstraintDrop() {
        Package pack = this.create(Package.class);
        Constraint constraint = this.create(Constraint.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraint, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Subject
        Class clazz = this.create(Class.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(constraint, clazz);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = this.create(Actor.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(constraint, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Interaction}.
     */
    @Test
    public void testInteractionDrop() {
        Package pack = this.create(Package.class);
        Interaction interaction = this.create(Interaction.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker
                .canDragAndDrop(interaction, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = this.create(Class.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(interaction,
                clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = this.create(Actor.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(interaction, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Package}.
     */
    @Test
    public void testPackageDrop() {
        Package pack = this.create(Package.class);
        Package pack2 = this.create(Package.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(pack,
                pack2);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = this.create(Actor.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(pack, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link StateMachine}.
     */
    @Test
    public void testStateMachineDrop() {
        Package pack = this.create(Package.class);
        StateMachine stateMachine = this.create(StateMachine.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker
                .canDragAndDrop(stateMachine, pack);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Subject
        Class clazz2 = this.create(Class.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(stateMachine,
                clazz2);
        assertFalse(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = this.create(Actor.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(stateMachine,
                actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link UseCase}.
     */
    @Test
    public void testUseCaseDrop() {
        Package pack = this.create(Package.class);
        UseCase useCase = this.create(UseCase.class);

        // authorized D&D on Package
        CheckStatus canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(useCase,
                pack);
        assertTrue(canDragAndDropStatus.isValid());

        // authorized D&D on Subject
        Class clazz = this.create(Class.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(useCase, clazz);
        assertTrue(canDragAndDropStatus.isValid());

        // not authorized D&D on Actor
        Actor actor = this.create(Actor.class);
        canDragAndDropStatus = this.useCaseExternalSourceToRepresentationDropChecker.canDragAndDrop(useCase, actor);
        assertFalse(canDragAndDropStatus.isValid());
    }

}
