/*******************************************************************************
 * Copyright (c) 2022 CEA, Obeo.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.modify;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.status.Status;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ConnectorEnd;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test changing the type of {@link Property} when connected to
 * {@link Connector} with {@link ConnectorEnd#getPartWithPort()}.
 * 
 * @author Arthur Daussy
 *
 */
//CHECKSTYLE:OFF
public class ElementFeatureSetterTest_SetTypeOnPropertyPartWithPort extends AbstractUMLTest {
    // CHECKSTYLE:ON
    private static final String TYPE = "type";
    private static final String OWNED_ATTRIBUTE = "ownedAttribute";
    private Class container;
    private Property property1;
    private Property property2;

    /**
     * Create a model with a connector connecting two {@link Property}s with using
     * {@link Port}.
     */
    @BeforeEach
    public void setUpModel() {
        Class type1 = create(Class.class);
        Port port1 = createIn(Port.class, type1, OWNED_ATTRIBUTE);

        Class type2 = create(Class.class);
        Port port2 = createIn(Port.class, type2, OWNED_ATTRIBUTE);

        container = create(Class.class);

        property1 = createIn(Property.class, container, OWNED_ATTRIBUTE);
        property1.setType(type1);
        property2 = createIn(Property.class, container, OWNED_ATTRIBUTE);
        property2.setType(type2);

        Connector connector = createIn(Connector.class, container, "ownedConnector");
        ConnectorEnd conEnd1 = createIn(ConnectorEnd.class, connector, "end");
        conEnd1.setRole(port1);
        conEnd1.setPartWithPort(property1);

        ConnectorEnd conEnd2 = createIn(ConnectorEnd.class, connector, "end");
        conEnd2.setRole(port2);
        conEnd2.setPartWithPort(property2);
    }

    /**
     * Setting the type to <code>null</code> should delete the connector.
     */
    @Test
    public void testSettingTypeToNull() {

        Status status = buildElementFeatureModifier().setValue(property2, TYPE, null);
        assertEquals(State.DONE, status.getState());
        assertTrue(container.getOwnedConnectors().isEmpty());

    }

    /**
     * Setting the type to another non compliant type should delete the connector.
     */
    @Test
    public void testSettingTypeToAnotherType() {

        Class newType = create(Class.class);

        Status status = buildElementFeatureModifier().setValue(property2, TYPE, newType);
        assertEquals(State.DONE, status.getState());
        assertTrue(container.getOwnedConnectors().isEmpty());

    }

    private ElementFeatureModifier buildElementFeatureModifier() {
        return new ElementFeatureModifier(getCrossRef(), getEditableChecker());
    }

    /**
     * Setting the type to another compliant type should <b>not</b> delete the
     * connector.
     */
    @Test
    public void testSettingTypeToAnotherCompliantType() {

        Class newType = create(Class.class);

        // The new type is a subtype of the existing type so the connector should not be
        // deleted
        Generalization generalization = createIn(Generalization.class, newType, "generalization");
        generalization.setGeneral((Classifier) property2.getType());

        assertTrue(newType.getSuperClasses().contains(property2.getType()));

        Status status = buildElementFeatureModifier().setValue(property2, TYPE, newType);
        assertEquals(State.DONE, status.getState());
        assertFalse(container.getOwnedConnectors().isEmpty());

    }

}
