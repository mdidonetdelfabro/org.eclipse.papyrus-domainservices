/*****************************************************************************
 * Copyright (c) 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.stream.Stream;

import org.eclipse.papyrus.uml.domain.services.drop.diagrams.UseCaseExternalSourceToRepresentationDropBehaviorProvider;
import org.eclipse.papyrus.uml.domain.services.status.State;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Element;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

/**
 * Test class for
 * {@link UseCaseExternalSourceToRepresentationDropBehaviorProvider}.
 *
 * @author <a href="mailto:gwendal.daniel@obeosoft.com">Gwendal DANIEL</a>
 */
public class UseCaseExternalSourceToRepresentationDropBehaviorProviderTest extends AbstractUMLTest {

    private UseCaseExternalSourceToRepresentationDropBehaviorProvider useCaseExternalSourceToRepresentationDropBehaviorProvider;

    @BeforeEach
    @Override
    public void setUp() {
        super.setUp();
        this.useCaseExternalSourceToRepresentationDropBehaviorProvider = new UseCaseExternalSourceToRepresentationDropBehaviorProvider();
    }

    /**
     * Test dropping an element into another.
     *
     * @param classToDrop
     *                        the type of the element to drop
     * @param containingClass
     *                        the type of the element containing the element to drop
     * @param expectedResult
     *                        true if NOTHING is expected, false if FAILED is
     *                        expected.
     *
     */
    @ParameterizedTest
    @MethodSource("getDragAndDropParameters")
    public void testDragAndDrop(Class<? extends Element> classToDrop, Class<? extends Element> containingClass,
            boolean expectedResult) {
        Element elementToDrop = this.create(classToDrop);
        Element containingElement = this.create(containingClass);

        DnDStatus status = this.useCaseExternalSourceToRepresentationDropBehaviorProvider.drop(elementToDrop,
                containingElement, this.getCrossRef(), this.getEditableChecker());
        State expectedState = State.FAILED;
        if (expectedResult) {
            assertTrue(List.of(elementToDrop).containsAll(status.getElementsToDisplay()));
            expectedState = State.NOTHING;
        }
        assertEquals(expectedState, status.getState());
    }

    public static Stream<Arguments> getDragAndDropParameters() {
        // @formatter:off
        return Stream.of(
                Arguments.of(Activity.class, Package.class, true),
                Arguments.of(Activity.class, Comment.class, false),
                Arguments.of(Actor.class, Package.class, true),
                Arguments.of(Actor.class, Comment.class, false),
                Arguments.of(org.eclipse.uml2.uml.Class.class, Package.class, true),
                Arguments.of(Comment.class, Package.class, true),
                Arguments.of(Comment.class, org.eclipse.uml2.uml.Class.class, true),
                Arguments.of(Comment.class, Comment.class, false),
                Arguments.of(Component.class, Package.class, true),
                Arguments.of(Component.class, Comment.class, false),
                Arguments.of(Constraint.class, Package.class, true),
                Arguments.of(Constraint.class, org.eclipse.uml2.uml.Class.class, true),
                Arguments.of(Constraint.class, Comment.class, false),
                Arguments.of(Interaction.class, Package.class, true),
                Arguments.of(Interaction.class, Comment.class, false),
                Arguments.of(Package.class, Package.class, true),
                Arguments.of(Package.class, Comment.class, false),
                // Relationships can be dropped anywhere on the diagram.
                Arguments.of(Extend.class, Package.class, true),
                Arguments.of(Extend.class, Comment.class, true),
                Arguments.of(StateMachine.class, Package.class, true),
                Arguments.of(StateMachine.class, Comment.class, false),
                Arguments.of(UseCase.class, Package.class, true),
                Arguments.of(UseCase.class, org.eclipse.uml2.uml.Class.class, true),
                Arguments.of(UseCase.class, Comment.class, false),
                Arguments.of(Artifact.class, Package.class, false)
                );
        // @formatter:on
    }

}
