/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.create;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertInstanceOf;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.AcceptCallAction;
import org.eclipse.uml2.uml.ActionExecutionSpecification;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.ActivityPartition;
import org.eclipse.uml2.uml.AddStructuralFeatureValueAction;
import org.eclipse.uml2.uml.AddVariableValueAction;
import org.eclipse.uml2.uml.AggregationKind;
import org.eclipse.uml2.uml.CallOperationAction;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.ClearAssociationAction;
import org.eclipse.uml2.uml.ClearStructuralFeatureAction;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CombinedFragment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.CreateLinkAction;
import org.eclipse.uml2.uml.CreateLinkObjectAction;
import org.eclipse.uml2.uml.CreateObjectAction;
import org.eclipse.uml2.uml.DestroyLinkAction;
import org.eclipse.uml2.uml.DestroyObjectAction;
import org.eclipse.uml2.uml.DurationConstraint;
import org.eclipse.uml2.uml.DurationInterval;
import org.eclipse.uml2.uml.ExecutionOccurrenceSpecification;
import org.eclipse.uml2.uml.ExecutionSpecification;
import org.eclipse.uml2.uml.ExpansionRegion;
import org.eclipse.uml2.uml.ExtensionEnd;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.InteractionOperand;
import org.eclipse.uml2.uml.Interval;
import org.eclipse.uml2.uml.IntervalConstraint;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.Pin;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.ReadExtentAction;
import org.eclipse.uml2.uml.ReadIsClassifiedObjectAction;
import org.eclipse.uml2.uml.ReadLinkAction;
import org.eclipse.uml2.uml.ReadSelfAction;
import org.eclipse.uml2.uml.ReadStructuralFeatureAction;
import org.eclipse.uml2.uml.ReadVariableAction;
import org.eclipse.uml2.uml.ReclassifyObjectAction;
import org.eclipse.uml2.uml.ReduceAction;
import org.eclipse.uml2.uml.SendObjectAction;
import org.eclipse.uml2.uml.SendSignalAction;
import org.eclipse.uml2.uml.StartClassifierBehaviorAction;
import org.eclipse.uml2.uml.StartObjectBehaviorAction;
import org.eclipse.uml2.uml.TestIdentityAction;
import org.eclipse.uml2.uml.TimeConstraint;
import org.eclipse.uml2.uml.TimeInterval;
import org.eclipse.uml2.uml.UMLFactory;
import org.eclipse.uml2.uml.UnmarshallAction;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.eclipse.uml2.uml.ValueSpecificationAction;
import org.junit.jupiter.api.Test;

public class ElementConfigurerTest extends AbstractUMLTest {

    private static final String OUTPUT_PIN_DEFAULT_NAME = "OutputPin1";
    private static final String INPUT_PIN_DEFAULT_NAME = "InputPin1";

    private static final String TARGET_ELEMENT_NAME = "target";
    private static final String INSERT_AT_ELEMENT_NAME = "insertAt";
    private static final String VALUE_ELEMENT_NAME = "value";
    private static final String RESULT_ELEMENT_NAME = "result";
    private static final String OBJECT_ELEMENT_NAME = "object";

    private UMLFactory fact = UMLFactory.eINSTANCE;

    @Test
    public void testNullInput() {
        assertNull(new ElementConfigurer().configure(null, null));
    }

    @Test
    public void testNamedElement() {

        var pack = this.create(Package.class);
        var c1 = this.create(Class.class);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(c1, pack);

        assertEquals("Class1", c1.getName()); //$NON-NLS-1$
        pack.getPackagedElements().add(c1);

        var c2 = this.fact.createClass();
        elementInitializer.configure(c2, pack);
        // Create a second one
        assertEquals("Class2", c2.getName()); //$NON-NLS-1$

        var c17 = this.fact.createClass();
        c17.setName("Class17"); //$NON-NLS-1$
        pack.getPackagedElements().add(c17);

        var c18 = this.fact.createClass();
        elementInitializer.configure(c18, pack);
        c17.setName("Class18"); //$NON-NLS-1$
    }

    @Test
    public void testPinCreation() {
        Pin inputPin = this.create(InputPin.class);
        Pin outputPin = this.create(OutputPin.class);
        ExpansionRegion expansionRegion = this.create(ExpansionRegion.class);
        ElementConfigurer elementConfigurer = new ElementConfigurer();
        elementConfigurer.configure(inputPin, expansionRegion);
        elementConfigurer.configure(outputPin, expansionRegion);
        assertEquals(INPUT_PIN_DEFAULT_NAME, inputPin.getName());
        assertTrue(inputPin.getInPartitions().isEmpty());
        assertEquals(OUTPUT_PIN_DEFAULT_NAME, outputPin.getName());
        assertTrue(outputPin.getInPartitions().isEmpty());
    }

    @Test
    public void testPinCreationContainerInPartition() {
        Pin inputPin = this.create(InputPin.class);
        Pin outputPin = this.create(OutputPin.class);
        ExpansionRegion expansionRegion = this.create(ExpansionRegion.class);
        ActivityPartition activityPartition = this.create(ActivityPartition.class);
        expansionRegion.getInPartitions().add(activityPartition);
        ElementConfigurer elementConfigurer = new ElementConfigurer();
        elementConfigurer.configure(inputPin, expansionRegion);
        elementConfigurer.configure(outputPin, expansionRegion);
        assertEquals(INPUT_PIN_DEFAULT_NAME, inputPin.getName());
        assertEquals(List.of(activityPartition), inputPin.getInPartitions());
        assertEquals(OUTPUT_PIN_DEFAULT_NAME, outputPin.getName());
        assertEquals(List.of(activityPartition), outputPin.getInPartitions());
    }

    @Test
    public void testPortCreation() {

        var c1 = this.create(Class.class);
        var port = this.create(Port.class);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(port, c1);

        assertEquals("Port1", port.getName()); //$NON-NLS-1$
        assertEquals(AggregationKind.COMPOSITE_LITERAL, port.getAggregation());
        c1.getOwnedPorts().add(port);

        var port2 = this.fact.createPort();
        elementInitializer.configure(port2, c1);

        // Create a second one
        assertEquals("Port2", port2.getName()); //$NON-NLS-1$
        assertEquals(AggregationKind.COMPOSITE_LITERAL, port2.getAggregation());

    }

    @Test
    public void testConstraintCreation() {

        var m1 = this.create(Model.class);
        var constraint = this.create(Constraint.class);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(constraint, m1);

        assertEquals("Constraint1", constraint.getName()); //$NON-NLS-1$
        assertEquals("constraintSpec", constraint.getSpecification().getName()); //$NON-NLS-1$
        m1.getOwnedRules().add(constraint);

        var constraint2 = this.fact.createConstraint();
        elementInitializer.configure(constraint2, m1);

        // Create a second one
        assertEquals("Constraint2", constraint2.getName()); //$NON-NLS-1$
        assertEquals("constraintSpec", constraint2.getSpecification().getName()); //$NON-NLS-1$
    }

    /**
     * Test the configuration of a usage => It should not have a default name.
     */
    @Test
    public void testUsageConfiguration() {
        var pack = this.create(Package.class);
        var usage = this.create(Usage.class);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(usage, pack);

        assertNull(usage.getName());
    }

    /**
     * Test the configuration of a UseCase => If its owner is a Classifier, useCase
     * should be added to useCase feature (subject feature of useCase should be
     * automatically updated).
     */
    @Test
    public void testUseCaseConfiguration() {
        Activity activity = this.create(Activity.class);
        UseCase useCase = this.create(UseCase.class);
        activity.getOwnedUseCases().add(useCase);

        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(useCase, activity);

        assertTrue(activity.getUseCases().contains(useCase));
        assertTrue(useCase.getSubjects().contains(activity));

        UseCase useCase2 = this.create(UseCase.class);
        Model model = this.create(Model.class);
        model.getPackagedElements().add(useCase2);
        elementInitializer.configure(useCase2, model);
        assertTrue(useCase2.getSubjects().isEmpty());
    }

    /**
     * Test the configuration of a Property => If its owner is a Collaboration, the
     * property should be added to collaborationRoles feature.
     */
    @Test
    public void testPropertyConfiguration() {
        Collaboration collaboration = this.create(Collaboration.class);
        Property property = this.create(Property.class);
        collaboration.getOwnedAttributes().add(property);
        var elementInitializer = new ElementConfigurer();
        elementInitializer.configure(property, collaboration);

        assertTrue(collaboration.getOwnedAttributes().contains(property));
        assertTrue(collaboration.getCollaborationRoles().contains(property));

        Property property2 = this.create(Property.class);
        Class class1 = this.create(Class.class);
        class1.getOwnedAttributes().add(property2);
        elementInitializer.configure(property2, class1);
        assertTrue(class1.getOwnedAttributes().contains(property2));
    }

    @Test
    public void testExtensionEndConfiguration() {
        ExtensionEnd extensionEnd = this.create(ExtensionEnd.class);
        Class clazz = this.create(Class.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(extensionEnd, clazz);

        assertEquals(AggregationKind.COMPOSITE_LITERAL, extensionEnd.getAggregation());
    }

    /**
     * Tests the configuration of an InteractionOperand => It should have an
     * InteractionConstraint set as guard named "guard".
     */
    @Test
    public void testInteractioOperandConfiguration() {
        CombinedFragment combinedFragment = this.create(CombinedFragment.class);
        InteractionOperand interactionOperand = this.createIn(InteractionOperand.class, combinedFragment);

        assertNull(interactionOperand.getGuard());

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(interactionOperand, combinedFragment);

        assertNotNull(interactionOperand.getGuard());
        assertEquals("guard", interactionOperand.getGuard().getName());
    }

    /**
     * A TimeConstraint should be initialized with a TimeInterval as specification
     * and min/max features of this TimeInterval have also been initialized and
     * created under the root package.
     */
    @Test
    public void testTimeConstraintConfiguration() {
        Model model = this.create(Model.class);
        Interaction interaction = this.createIn(Interaction.class, model);
        TimeConstraint timeConstraint = this.create(TimeConstraint.class);
        interaction.getPreconditions().add(timeConstraint);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(timeConstraint, interaction);

        assertNotNull(timeConstraint.getSpecification());
        assertEquals("TimeInterval", timeConstraint.getSpecification().eClass().getName());
        TimeInterval timeInterval = (TimeInterval) timeConstraint.getSpecification();
        assertNotNull(timeInterval.getMin());
        assertNotNull(timeInterval.getMax());
        assertTrue(model.getPackagedElements().contains(timeInterval.getMin()));
        assertTrue(model.getPackagedElements().contains(timeInterval.getMax()));
    }

    /**
     * If there is no root package, the min/max features of the TimeInterval should
     * not be initialized.
     */
    @Test
    public void testTimeConstraintConfigurationWithoutModel() {
        Interaction interaction = this.create(Interaction.class);
        TimeConstraint timeConstraint = this.create(TimeConstraint.class);
        interaction.getPreconditions().add(timeConstraint);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(timeConstraint, interaction);

        assertNotNull(timeConstraint.getSpecification());
        assertEquals("TimeInterval", timeConstraint.getSpecification().eClass().getName());
        TimeInterval timeInterval = (TimeInterval) timeConstraint.getSpecification();
        assertNull(timeInterval.getMin());
        assertNull(timeInterval.getMax());
    }

    /**
     * An {@link ActionExecutionSpecification} should be initialized with a name and
     * start/finish {@link ExecutionOccurrenceSpecification}.
     * <p>
     * The {@link ExecutionOccurrenceSpecification} should be added to the
     * execution's owner fragments.
     */
    @Test
    public void testActionExecutionSpecificationConfiguration() {
        Interaction interaction = this.create(Interaction.class);
        ExecutionSpecification executionSpecification = this.create(ActionExecutionSpecification.class);
        interaction.getFragments().add(executionSpecification);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(executionSpecification, interaction);

        assertNotNull(executionSpecification.getName());
        String executionSpecificationName = executionSpecification.getName();

        assertNotNull(executionSpecification.getStart());
        assertInstanceOf(ExecutionOccurrenceSpecification.class, executionSpecification.getStart());
        ExecutionOccurrenceSpecification start = (ExecutionOccurrenceSpecification) executionSpecification.getStart();
        assertEquals(executionSpecification, start.getExecution());
        assertEquals(executionSpecificationName + "Start", start.getName());
        assertTrue(interaction.getFragments().contains(start));

        assertNotNull(executionSpecification.getFinish());
        assertInstanceOf(ExecutionOccurrenceSpecification.class, executionSpecification.getFinish());
        ExecutionOccurrenceSpecification finish = (ExecutionOccurrenceSpecification) executionSpecification.getFinish();
        assertEquals(executionSpecification, finish.getExecution());
        assertEquals(executionSpecificationName + "Finish", finish.getName());
        assertTrue(interaction.getFragments().contains(finish));

    }

    /**
     * A DurationConstraint should be initialized with a DurationInterval as
     * specification and min/max features of this DurationInterval have also been
     * initialized and created under the root package.
     */
    @Test
    public void testDurationConstraintConfiguration() {
        Model model = this.create(Model.class);
        Interaction interaction = this.createIn(Interaction.class, model);
        DurationConstraint durationConstraint = this.create(DurationConstraint.class);
        interaction.getPreconditions().add(durationConstraint);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(durationConstraint, interaction);

        assertNotNull(durationConstraint.getSpecification());
        assertEquals("DurationInterval", durationConstraint.getSpecification().eClass().getName());
        DurationInterval durationInterval = (DurationInterval) durationConstraint.getSpecification();
        assertNotNull(durationInterval.getMin());
        assertNotNull(durationInterval.getMax());
        assertTrue(model.getPackagedElements().contains(durationInterval.getMin()));
        assertTrue(model.getPackagedElements().contains(durationInterval.getMax()));
    }

    /**
     * If there is no root package, the min/max features of the DurationInterval
     * should not be initialized.
     */
    @Test
    public void testDurationConstraintConfigurationWithoutModel() {
        Interaction interaction = this.create(Interaction.class);
        DurationConstraint durationConstraint = this.create(DurationConstraint.class);
        interaction.getPreconditions().add(durationConstraint);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(durationConstraint, interaction);

        assertNotNull(durationConstraint.getSpecification());
        assertEquals("DurationInterval", durationConstraint.getSpecification().eClass().getName());
        DurationInterval durationInterval = (DurationInterval) durationConstraint.getSpecification();
        assertNull(durationInterval.getMin());
        assertNull(durationInterval.getMax());
    }

    /**
     * An IntervalConstraint should be initialized with an Interval as specification
     * and min/max features of this Interval have also been initialized and created
     * under the root package.
     */
    @Test
    public void testIntervalConstraintConfiguration() {
        Model model = this.create(Model.class);
        Interaction interaction = this.createIn(Interaction.class, model);
        IntervalConstraint intervalConstraint = this.create(IntervalConstraint.class);
        interaction.getPreconditions().add(intervalConstraint);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(intervalConstraint, interaction);

        assertNotNull(intervalConstraint.getSpecification());
        assertEquals("Interval", intervalConstraint.getSpecification().eClass().getName());
        Interval interval = (Interval) intervalConstraint.getSpecification();
        assertNotNull(interval.getMin());
        assertNotNull(interval.getMax());
        assertTrue(model.getPackagedElements().contains(interval.getMin()));
        assertTrue(model.getPackagedElements().contains(interval.getMax()));
    }

    /**
     * If there is no root package, the min/max features of the Interval should not
     * be initialized.
     */
    @Test
    public void testIntervalConstraintConfigurationWithoutModel() {
        Interaction interaction = this.create(Interaction.class);
        IntervalConstraint intervalConstraint = this.create(IntervalConstraint.class);
        interaction.getPreconditions().add(intervalConstraint);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(intervalConstraint, interaction);

        assertNotNull(intervalConstraint.getSpecification());
        assertEquals("Interval", intervalConstraint.getSpecification().eClass().getName());
        Interval interval = (Interval) intervalConstraint.getSpecification();
        assertNull(interval.getMin());
        assertNull(interval.getMax());
    }

    @Test
    public void testAcceptCallActionPinsConfiguration() {
        AcceptCallAction acceptCallAction = this.create(AcceptCallAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(acceptCallAction, null);

        OutputPin returnInformation = acceptCallAction.getReturnInformation();
        assertNotNull(returnInformation);
        assertEquals("return information", returnInformation.getName());
    }

    @Test
    public void testAddStructuralFeatureValueActionPinsConfiguration() {
        AddStructuralFeatureValueAction addStructuralFeatureValueAction = this
                .create(AddStructuralFeatureValueAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(addStructuralFeatureValueAction, null);

        InputPin object = addStructuralFeatureValueAction.getObject();
        InputPin value = addStructuralFeatureValueAction.getValue();
        InputPin insertAt = addStructuralFeatureValueAction.getInsertAt();
        OutputPin result = addStructuralFeatureValueAction.getResult();
        assertNotNull(object);
        assertNotNull(value);
        assertNotNull(insertAt);
        assertNotNull(result);
        assertEquals(OBJECT_ELEMENT_NAME, object.getName());
        assertEquals(VALUE_ELEMENT_NAME, value.getName());
        assertEquals(INSERT_AT_ELEMENT_NAME, insertAt.getName());
        assertEquals(RESULT_ELEMENT_NAME, result.getName());
    }

    @Test
    public void testAddVariableValueActionPinsConfiguration() {
        AddVariableValueAction addVariableValueAction = this.create(AddVariableValueAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(addVariableValueAction, null);

        InputPin value = addVariableValueAction.getValue();
        InputPin insertAt = addVariableValueAction.getInsertAt();
        assertNotNull(value);
        assertNotNull(insertAt);
        assertEquals(VALUE_ELEMENT_NAME, value.getName());
        assertEquals(INSERT_AT_ELEMENT_NAME, insertAt.getName());
    }

    @Test
    public void testCallOperationActionPinsConfiguration() {
        CallOperationAction callOperationAction = this.create(CallOperationAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(callOperationAction, null);

        InputPin target = callOperationAction.getTarget();
        assertNotNull(target);
        assertEquals(TARGET_ELEMENT_NAME, target.getName());
    }

    @Test
    public void testClearAssociationActionPinsConfiguration() {
        ClearAssociationAction clearAssociationAction = this.create(ClearAssociationAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(clearAssociationAction, null);

        InputPin object = clearAssociationAction.getObject();
        assertNotNull(object);
        assertEquals(OBJECT_ELEMENT_NAME, object.getName());
    }

    @Test
    public void testClearStructuralFeatureActionPinsConfiguration() {
        ClearStructuralFeatureAction clearStructuralFeatureAction = this.create(ClearStructuralFeatureAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(clearStructuralFeatureAction, null);

        InputPin object = clearStructuralFeatureAction.getObject();
        OutputPin result = clearStructuralFeatureAction.getResult();
        assertNotNull(object);
        assertNotNull(result);
        assertEquals(OBJECT_ELEMENT_NAME, object.getName());
        assertEquals(RESULT_ELEMENT_NAME, result.getName());
    }

    @Test
    public void testCreateLinkActionPinsConfiguration() {
        CreateLinkAction createLinkAction = this.create(CreateLinkAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(createLinkAction, null);

        assertFalse(createLinkAction.getInputValues().isEmpty());
        InputPin inputValue = createLinkAction.getInputValues().get(0);
        assertNotNull(inputValue);
        assertEquals(INPUT_PIN_DEFAULT_NAME, inputValue.getName());
    }

    @Test
    public void testCreateLinkObjectActionPinsConfiguration() {
        CreateLinkObjectAction createLinkObjectAction = this.create(CreateLinkObjectAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(createLinkObjectAction, null);

        assertFalse(createLinkObjectAction.getInputValues().isEmpty());
        InputPin inputValue = createLinkObjectAction.getInputValues().get(0);
        OutputPin result = createLinkObjectAction.getResult();
        assertNotNull(inputValue);
        assertNotNull(result);
        assertEquals(INPUT_PIN_DEFAULT_NAME, inputValue.getName());
        assertEquals(RESULT_ELEMENT_NAME, result.getName());
    }

    @Test
    public void testCreateObjectActionPinsConfiguration() {
        CreateObjectAction createObjectAction = this.create(CreateObjectAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(createObjectAction, null);

        OutputPin result = createObjectAction.getResult();
        assertNotNull(result);
        assertEquals(RESULT_ELEMENT_NAME, result.getName());
    }

    @Test
    public void testDestroyLinkActionPinsConfiguration() {
        DestroyLinkAction destroyLinkAction = this.create(DestroyLinkAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(destroyLinkAction, null);

        assertFalse(destroyLinkAction.getInputValues().isEmpty());
        InputPin inputValue = destroyLinkAction.getInputValues().get(0);
        assertNotNull(inputValue);
        assertEquals(INPUT_PIN_DEFAULT_NAME, inputValue.getName());
    }

    @Test
    public void testDestroyObjectActionPinsConfiguration() {
        DestroyObjectAction destroyObjectAction = this.create(DestroyObjectAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(destroyObjectAction, null);

        InputPin target = destroyObjectAction.getTarget();
        assertNotNull(target);
        assertEquals(TARGET_ELEMENT_NAME, target.getName());
    }

    @Test
    public void testReadExtentActionPinsConfiguration() {
        ReadExtentAction readExtentAction = this.create(ReadExtentAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(readExtentAction, null);

        OutputPin result = readExtentAction.getResult();
        assertNotNull(result);
        assertEquals(RESULT_ELEMENT_NAME, result.getName());
    }

    @Test
    public void testReadIsClassifiedObjectActionPinsConfiguration() {
        ReadIsClassifiedObjectAction readIsClassifiedObjectAction = this.create(ReadIsClassifiedObjectAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(readIsClassifiedObjectAction, null);

        InputPin object = readIsClassifiedObjectAction.getObject();
        OutputPin result = readIsClassifiedObjectAction.getResult();
        assertNotNull(object);
        assertNotNull(result);
        assertEquals(OBJECT_ELEMENT_NAME, object.getName());
        assertEquals(RESULT_ELEMENT_NAME, result.getName());
    }

    @Test
    public void testReadLinkActionPinsConfiguration() {
        ReadLinkAction readLinkAction = this.create(ReadLinkAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(readLinkAction, null);

        assertFalse(readLinkAction.getInputValues().isEmpty());
        InputPin inputValue = readLinkAction.getInputValues().get(0);
        OutputPin result = readLinkAction.getResult();
        assertNotNull(inputValue);
        assertNotNull(result);
        assertEquals(INPUT_PIN_DEFAULT_NAME, inputValue.getName());
        assertEquals(RESULT_ELEMENT_NAME, result.getName());
    }

    @Test
    public void testReadSelfActionPinsConfiguration() {
        ReadSelfAction readSelfAction = this.create(ReadSelfAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(readSelfAction, null);

        OutputPin result = readSelfAction.getResult();
        assertNotNull(result);
        assertEquals(RESULT_ELEMENT_NAME, result.getName());
    }

    @Test
    public void testReadStructuralFeatureActionPinsConfiguration() {
        ReadStructuralFeatureAction readStructuralFeatureAction = this.create(ReadStructuralFeatureAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(readStructuralFeatureAction, null);

        InputPin object = readStructuralFeatureAction.getObject();
        OutputPin result = readStructuralFeatureAction.getResult();
        assertNotNull(object);
        assertNotNull(result);
        assertEquals(OBJECT_ELEMENT_NAME, object.getName());
        assertEquals(RESULT_ELEMENT_NAME, result.getName());
    }

    @Test
    public void testReadVariableActionPinsConfiguration() {
        ReadVariableAction readVariableAction = this.create(ReadVariableAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(readVariableAction, null);

        OutputPin result = readVariableAction.getResult();
        assertNotNull(result);
        assertEquals(RESULT_ELEMENT_NAME, result.getName());
    }

    @Test
    public void testReclassifyObjectActionPinsConfiguration() {
        ReclassifyObjectAction reclassifyObjectAction = this.create(ReclassifyObjectAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(reclassifyObjectAction, null);

        InputPin object = reclassifyObjectAction.getObject();
        assertNotNull(object);
        assertEquals(OBJECT_ELEMENT_NAME, object.getName());
    }

    @Test
    public void testReduceActionPinsConfiguration() {
        ReduceAction reduceAction = this.create(ReduceAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(reduceAction, null);

        OutputPin result = reduceAction.getResult();
        InputPin collection = reduceAction.getCollection();
        assertNotNull(result);
        assertNotNull(collection);
        assertEquals(RESULT_ELEMENT_NAME, result.getName());
        assertEquals("collection", collection.getName());
    }

    @Test
    public void testSendObjectActionPinsConfiguration() {
        SendObjectAction sendObjectAction = this.create(SendObjectAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(sendObjectAction, null);

        InputPin request = sendObjectAction.getRequest();
        InputPin target = sendObjectAction.getTarget();
        assertNotNull(request);
        assertNotNull(target);
        assertEquals("request", request.getName());
        assertEquals(TARGET_ELEMENT_NAME, target.getName());
    }

    @Test
    public void testSendSignalActionPinsConfiguration() {
        SendSignalAction sendSignalAction = this.create(SendSignalAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(sendSignalAction, null);

        InputPin target = sendSignalAction.getTarget();
        assertNotNull(target);
        assertEquals(TARGET_ELEMENT_NAME, target.getName());
    }

    @Test
    public void testStartClassifierBehaviorActionPinsConfiguration() {
        StartClassifierBehaviorAction startClassifierBehaviorAction = this.create(StartClassifierBehaviorAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(startClassifierBehaviorAction, null);

        InputPin object = startClassifierBehaviorAction.getObject();
        assertNotNull(object);
        assertEquals(OBJECT_ELEMENT_NAME, object.getName());
    }

    @Test
    public void testStartObjectBehaviorActionPinsConfiguration() {
        StartObjectBehaviorAction startObjectBehaviorAction = this.create(StartObjectBehaviorAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(startObjectBehaviorAction, null);

        InputPin object = startObjectBehaviorAction.getObject();
        assertNotNull(object);
        assertEquals(OBJECT_ELEMENT_NAME, object.getName());
    }

    @Test
    public void testTestIdentityActionPinsConfiguration() {
        TestIdentityAction testIdentityAction = this.create(TestIdentityAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(testIdentityAction, null);

        InputPin first = testIdentityAction.getFirst();
        InputPin second = testIdentityAction.getSecond();
        OutputPin result = testIdentityAction.getResult();
        assertNotNull(first);
        assertNotNull(second);
        assertNotNull(result);
        assertEquals("first", first.getName());
        assertEquals("second", second.getName());
        assertEquals(RESULT_ELEMENT_NAME, result.getName());
    }

    @Test
    public void testUnmarshallActionPinsConfiguration() {
        UnmarshallAction unmarshallAction = this.create(UnmarshallAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(unmarshallAction, null);

        assertFalse(unmarshallAction.getResults().isEmpty());
        InputPin object = unmarshallAction.getObject();
        OutputPin result = unmarshallAction.getResults().get(0);
        assertNotNull(object);
        assertNotNull(result);
        assertEquals(OBJECT_ELEMENT_NAME, object.getName());
        assertEquals(OUTPUT_PIN_DEFAULT_NAME, result.getName());
    }

    @Test
    public void testValueSpecificationActionPinsConfiguration() {
        ValueSpecificationAction valueSpecificationAction = this.create(ValueSpecificationAction.class);

        ElementConfigurer configurer = new ElementConfigurer();
        configurer.configure(valueSpecificationAction, null);

        OutputPin result = valueSpecificationAction.getResult();
        assertNotNull(result);
        assertEquals(RESULT_ELEMENT_NAME, result.getName());
    }

}
