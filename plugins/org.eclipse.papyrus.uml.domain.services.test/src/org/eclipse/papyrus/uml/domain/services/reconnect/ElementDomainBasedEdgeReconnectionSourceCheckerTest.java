/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.reconnect;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.papyrus.uml.domain.services.edges.ElementDomainBasedEdgeInitializer;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.papyrus.uml.domain.services.utils.MockedViewQuerier;
import org.eclipse.papyrus.uml.domain.services.utils.VisualNode;
import org.eclipse.papyrus.uml.domain.services.utils.VisualTree;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Actor;
import org.eclipse.uml2.uml.Artifact;
import org.eclipse.uml2.uml.Association;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Classifier;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.CommunicationPath;
import org.eclipse.uml2.uml.Component;
import org.eclipse.uml2.uml.ComponentRealization;
import org.eclipse.uml2.uml.Connector;
import org.eclipse.uml2.uml.ControlFlow;
import org.eclipse.uml2.uml.CreateObjectAction;
import org.eclipse.uml2.uml.Dependency;
import org.eclipse.uml2.uml.DeployedArtifact;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.DeploymentTarget;
import org.eclipse.uml2.uml.Extend;
import org.eclipse.uml2.uml.Extension;
import org.eclipse.uml2.uml.FlowFinalNode;
import org.eclipse.uml2.uml.Generalization;
import org.eclipse.uml2.uml.Include;
import org.eclipse.uml2.uml.InformationFlow;
import org.eclipse.uml2.uml.InputPin;
import org.eclipse.uml2.uml.InstanceSpecification;
import org.eclipse.uml2.uml.Interface;
import org.eclipse.uml2.uml.Lifeline;
import org.eclipse.uml2.uml.Manifestation;
import org.eclipse.uml2.uml.MergeNode;
import org.eclipse.uml2.uml.Message;
import org.eclipse.uml2.uml.Model;
import org.eclipse.uml2.uml.NamedElement;
import org.eclipse.uml2.uml.Node;
import org.eclipse.uml2.uml.ObjectFlow;
import org.eclipse.uml2.uml.OpaqueAction;
import org.eclipse.uml2.uml.OutputPin;
import org.eclipse.uml2.uml.Package;
import org.eclipse.uml2.uml.PackageImport;
import org.eclipse.uml2.uml.PackageMerge;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.PrimitiveType;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.Pseudostate;
import org.eclipse.uml2.uml.ReclassifyObjectAction;
import org.eclipse.uml2.uml.Region;
import org.eclipse.uml2.uml.State;
import org.eclipse.uml2.uml.Stereotype;
import org.eclipse.uml2.uml.Substitution;
import org.eclipse.uml2.uml.Transition;
import org.eclipse.uml2.uml.Usage;
import org.eclipse.uml2.uml.UseCase;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

/**
 * Test class for {@link ElementDomainBasedEdgeReconnectionSourceChecker}.
 *
 * @author <a href="mailto:jessy.mallet@obeo.fr">Jessy Mallet</a>
 *
 */
public class ElementDomainBasedEdgeReconnectionSourceCheckerTest extends AbstractUMLTest {

    private static final String OWNED_ATTRIBUTE = "ownedAttribute";

    private ElementDomainBasedEdgeReconnectionSourceChecker elementDomainBasedEdgeReconnectionSourceChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        this.elementDomainBasedEdgeReconnectionSourceChecker = new ElementDomainBasedEdgeReconnectionSourceChecker(
                e -> true, new MockedViewQuerier());
    }

    /**
     * Check that the reconnect source is not authorized when we try to reconnect an
     * association from a class to an other classifier which cannot contain
     * attribute (source property is not owned by the association).
     */
    @Test
    public void testCannotReconnectAssociationSourceWithPropertyNotOwnedByAssociation() {
        Model model = this.create(Model.class);
        Class classOldSource = this.createIn(Class.class, model);
        Class classTarget = this.createIn(Class.class, model);
        Association association = this.createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classOldSource, classTarget, null, null, null);

        // change owner of source Property
        Property sourceProperty = association.getMemberEnds().get(0);
        classOldSource.getOwnedAttributes().add(sourceProperty);
        Actor actorNewSource = this.createIn(Actor.class, model);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(association,
                classOldSource, actorNewSource, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Check that the reconnect source is authorized when we try to reconnect an
     * association from a class to an other classifier which can contain attribute
     * (source property is not owned by the association).
     */
    @Test
    public void testCanReconnectAssociationSourceWithPropertyNotOwnedByAssociation() {
        Model model = this.create(Model.class);
        Class classOldSource = this.createIn(Class.class, model);
        Class classTarget = this.createIn(Class.class, model);
        Association association = this.createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, classOldSource, classTarget, null, null, null);

        // change owner of source Property
        Property sourceProperty = association.getMemberEnds().get(0);
        classOldSource.getOwnedAttributes().add(sourceProperty);
        Class classNewSource = this.createIn(Class.class, model);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(association,
                classOldSource, classNewSource, null, null);
        assertTrue(status.isValid());
    }

    /**
     * Check that the reconnect source is authorized when we try to reconnect an
     * association from a class to an other classifier (source property is owned by
     * the association).
     */
    @Test
    public void testCanReconnectAssociationSourceWithPropertyOwnedByAssociation() {
        Model model = this.create(Model.class);
        UseCase sourceUseCase = this.createIn(UseCase.class, model);
        UseCase targetUseCase = this.createIn(UseCase.class, model);
        UseCase newSourceUseCase = this.createIn(UseCase.class, model);
        Association association = this.createIn(Association.class, model);
        new ElementDomainBasedEdgeInitializer().initialize(association, sourceUseCase, targetUseCase, null, null, null);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(association,
                sourceUseCase, newSourceUseCase, null, null);
        assertTrue(status.isValid());

        Comment newSourceComment = this.createIn(Comment.class, model);
        status = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(association, sourceUseCase,
                newSourceComment, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Tests that a {@link CommunicationPath} reconnection with a
     * {@link DeploymentTarget} and {@link Classifier} element as source is allowed.
     */
    @Test
    public void testCanReconnectCommunicationPathSourceWithDeploymentTargetClassifier() {
        CommunicationPath communicationPath = this.create(CommunicationPath.class);
        Node source = this.create(Node.class);
        Node target = this.create(Node.class);
        new ElementDomainBasedEdgeInitializer().initialize(communicationPath, source, target, null, null, null);

        Node newSource = this.create(Node.class);
        CheckStatus status = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(communicationPath,
                source, newSource, null, null);
        assertTrue(status.isValid());
    }

    /**
     * Tests that a {@link CommunicationPath} reconnection with a
     * {@link Classifier}, non-{@link DeploymentTarget} element as source is not
     * allowed.
     */
    @Test
    public void testCanReconnectCommunicationPathSourceWithClassifierNonDeploymentTarget() {
        CommunicationPath communicationPath = this.create(CommunicationPath.class);
        Node source = this.create(Node.class);
        Node target = this.create(Node.class);
        new ElementDomainBasedEdgeInitializer().initialize(communicationPath, source, target, null, null, null);
        Class newSource = this.create(Class.class);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(communicationPath,
                source, newSource, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Tests that a {@link CommunicationPath} reconnection with a
     * {@link DeploymentTarget}, non-{@link Classifier} element as source is not
     * allowed.
     */
    @Test
    public void testReconnectCommunicationPathSourceWithDeploymentTargetNonClassifier() {
        CommunicationPath communicationPath = this.create(CommunicationPath.class);
        Node source = this.create(Node.class);
        Node target = this.create(Node.class);
        new ElementDomainBasedEdgeInitializer().initialize(communicationPath, source, target, null, null, null);
        InstanceSpecification newSource = this.create(InstanceSpecification.class);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(communicationPath,
                source, newSource, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Tests that a {@link CommunicationPath} reconnection with {@code null} as
     * source is not allowed.
     */
    @Test
    public void testCanReconnectCommunicationPathSourceWithNull() {
        CommunicationPath communicationPath = this.create(CommunicationPath.class);
        Node source = this.create(Node.class);
        Node target = this.create(Node.class);
        new ElementDomainBasedEdgeInitializer().initialize(communicationPath, source, target, null, null, null);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(communicationPath,
                source, null, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Tests that a {@link ComponentRealization} reconnection with a classifier
     * element as source is allowed.
     */
    @Test
    public void testCanReconnectComponentRealizationSourceWithClassifier() {
        ComponentRealization componentRealization = this.create(ComponentRealization.class);
        Interface sourceInterface = this.create(Interface.class);
        Component targetComponent = this.create(Component.class);
        componentRealization.getRealizingClassifiers().add(sourceInterface);
        componentRealization.setAbstraction(targetComponent);

        Classifier newSource = this.create(Interface.class);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(componentRealization,
                sourceInterface, newSource, null, null);
        assertTrue(status.isValid());
    }

    /**
     * Tests that a {@link ComponentRealization} reconnection with a non-classifier
     * source is not allowed.
     */
    @Test
    public void testCanReconnectComponentRealizationSourceWithNonClassifier() {
        ComponentRealization componentRealization = this.create(ComponentRealization.class);
        Interface sourceInterface = this.create(Interface.class);
        Component targetComponent = this.create(Component.class);
        componentRealization.getRealizingClassifiers().add(sourceInterface);
        componentRealization.setAbstraction(targetComponent);

        Comment newSource = this.create(Comment.class);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(componentRealization,
                sourceInterface, newSource, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Tests that a {@link ComponentRealization} reconnection with a null source is
     * not allowed.
     */
    @Test
    public void testCanReconnectComponentRealizationSourceWithNullSource() {
        ComponentRealization componentRealization = this.create(ComponentRealization.class);
        Interface sourceInterface = this.create(Interface.class);
        Component targetComponent = this.create(Component.class);
        componentRealization.getRealizingClassifiers().add(sourceInterface);
        componentRealization.setAbstraction(targetComponent);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(componentRealization,
                sourceInterface, null, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Tests that a {@link ComponentRealization} reconnection with the target
     * element as source is not allowed.
     */
    @Test
    public void testCanReconnectComponentRealizationSourceWithTargetElementAsSource() {
        ComponentRealization componentRealization = this.create(ComponentRealization.class);
        Interface sourceInterface = this.create(Interface.class);
        Component targetComponent = this.create(Component.class);
        componentRealization.getRealizingClassifiers().add(sourceInterface);
        componentRealization.setAbstraction(targetComponent);

        CheckStatus status = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(componentRealization,
                sourceInterface, targetComponent, null, null);
        assertFalse(status.isValid());
    }

    /**
     * Check that the reconnect is authorized when we try to reconnect a connector
     * between two different properties not contained in each other.
     */
    @Test
    public void testCanReconnectConnectorSourceFrom2DifferentProperty() {
        // create semantic elements
        Class clazz = this.create(Class.class);
        Property propertySource = this.createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Property propertyTarget = this.createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Property oldProperySource = this.createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        Connector connector = this.create(Connector.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPropertySource = classNode.addChildren(propertySource);
        VisualNode visualPropertyTarget = classNode.addChildren(propertyTarget);

        // check reconnect is authorized
        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(connector,
                oldProperySource, propertySource, visualPropertySource, visualPropertyTarget);
        assertTrue(canReconnectStatus.isValid());
    }

    /**
     * Check that the reconnect is unauthorized when we try to reconnect a connector
     * from a port view to its self.
     */
    @Test
    public void testCanReconnectConnectorSourceFromPortOnItself() {
        // create semantic elements
        Class clazz = this.create(Class.class);
        Port newPortSource = this.createIn(Port.class, clazz, OWNED_ATTRIBUTE);
        Port oldPortSource = this.createIn(Port.class, clazz, OWNED_ATTRIBUTE);
        Connector connector = this.createIn(Connector.class, clazz);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPort = classNode.addChildren(newPortSource);

        // check reconnect is unauthorized
        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(connector,
                oldPortSource, newPortSource, visualPort, visualPort);
        assertFalse(canReconnectStatus.isValid());
        assertEquals("Cannot connect a port to itself.", canReconnectStatus.getMessage());
    }

    /**
     * Check that the reconnect is unauthorized when we try to reconnect a connector
     * between a port view and a property view with the port as border node.
     */
    @Test
    public void testCanReconnectConnectorSourceFromPortOnItsPropertyContainerNode() {
        // create semantic elements
        Class type1 = this.create(Class.class);
        Property newPropertySource = this.create(Property.class);
        newPropertySource.setType(type1);
        Port portTarget = this.createIn(Port.class, type1, OWNED_ATTRIBUTE);
        Port oldPortSource = this.createIn(Port.class, type1, OWNED_ATTRIBUTE);
        Connector connector = this.create(Connector.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode newPropertyNode = visualTree.addChildren(newPropertySource);
        VisualNode visualTargetPort = newPropertyNode.addBorderNode(portTarget);

        // check reconnect is unauthorized
        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(connector,
                oldPortSource, newPropertySource, newPropertyNode, visualTargetPort);
        assertFalse(canReconnectStatus.isValid());
        assertEquals("Cannot create a connector from a view representing a Part to its own Port (or the opposite).",
                canReconnectStatus.getMessage());
    }

    /**
     * Check that the reconnect is unauthorized when we try to reconnect a connector
     * between a property view contained by an other one.
     */
    @Test
    public void testCanReconnectConnectorSourceFromPropertyContainedInAnotherOne() {
        // create semantic elements
        Class type1 = this.create(Class.class);
        Class clazz = this.create(Class.class);
        Property oldPropertySource = this.createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        oldPropertySource.setType(type1);
        Property propertyTarget = this.createIn(Property.class, clazz, OWNED_ATTRIBUTE);
        propertyTarget.setType(type1);
        Property newPropertySource = this.createIn(Property.class, type1, OWNED_ATTRIBUTE);
        Connector connector = this.create(Connector.class);

        // Create views
        VisualTree visualTree = new VisualTree();
        VisualNode classNode = visualTree.addChildren(clazz);
        VisualNode visualPropertyTarget = classNode.addChildren(propertyTarget);
        VisualNode visualNewPropertySource = visualPropertyTarget.addChildren(newPropertySource);

        // check reconnect is unauthorized
        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(connector,
                oldPropertySource, newPropertySource, visualNewPropertySource, visualPropertyTarget);
        assertFalse(canReconnectStatus.isValid());
        assertEquals(
                "Cannot connect a Part to one of its (possibly indirect) containment, must connect to one of its Port.",
                canReconnectStatus.getMessage());
    }

    /**
     * Check the canReconnect with {@link ControlFlow}. This test aims to check if
     * the checker mechanism is properly called. This test will not verify all cases
     * since they are already verified by the
     * ElementDomainBasedEdgeCreationCheckerTest.testCanCreateControlFlowSource
     * test. The
     * org.eclipse.papyrus.uml.domain.services.objectFlow.ControlFlowHelper.canCreateControlFlow
     * method is called by both ElementDomainBasedEdgeCreationChecker and
     * ElementDomainBasedEdgeReconnectionSourceChecker.
     */
    @Test
    public void testCanReconnectControlFlowSource() {
        // Check the basic case
        MergeNode mergeNode = this.create(MergeNode.class);
        MergeNode newMergeNode = this.create(MergeNode.class);
        OpaqueAction opaqueAction = this.create(OpaqueAction.class);
        ControlFlow controlFlow = this.create(ControlFlow.class);
        controlFlow.setSource(mergeNode);
        controlFlow.setTarget(opaqueAction);
        // Check an authorize case.
        assertTrue(new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false, new MockedViewQuerier())
                .canReconnect(controlFlow, mergeNode, newMergeNode, null, null).isValid());

        // Check a forbidden case.
        Activity activity = this.create(Activity.class);
        assertFalse(new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false, new MockedViewQuerier())
                .canReconnect(controlFlow, mergeNode, activity, null, null).isValid());
    }

    /**
     * Test reconnect source with a named element => reconnection is authorized.
     */
    @Test
    public void testCanReconnectDependencySourceWithNamedElement() {
        // create semantic elements
        Dependency dependency = this.create(Dependency.class);
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        Class c3 = this.create(Class.class);

        // check creation is authorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(dependency, c1,
                c3, null, null);
        assertTrue(canCreateStatus.isValid());
    }

    /**
     * Test reconnect source with no namedElement => reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDependencySourceWithNoNamedElement() {
        // create semantic elements
        Dependency dependency = this.create(Dependency.class);
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);
        Comment comment = this.create(Comment.class);

        // check creation is unauthorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(dependency, c1,
                comment, null, null);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect source with null source => reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDependencySourceWithNullSource() {
        // create semantic elements
        Dependency dependency = this.create(Dependency.class);
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);
        dependency.getClients().add(c1);
        dependency.getSuppliers().add(c2);

        // check creation is unauthorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(dependency, c1,
                null, null, null);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect source with {@link DeployedArtifact} source => reconnection is
     * authorized.
     */
    @Test
    public void testCanReconnectDeploymentSourceWithDeployedArtifactSource() {
        Deployment deployment = this.create(Deployment.class);
        Artifact artifact = this.create(Artifact.class);
        Artifact artifact2 = this.create(Artifact.class);
        Node node = this.create(Node.class);
        deployment.getDeployedArtifacts().add(artifact);
        deployment.setLocation(node);

        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(deployment,
                artifact, artifact2, null, null);
        assertTrue(canReconnectStatus.isValid());
    }

    /**
     * Test reconnect source with non-{@link DeployedArtifact} source =>
     * reconnection is not authorized.
     */
    @Test
    public void testCanReconnectDeploymentSourceWithNonDeployedArtifactSource() {
        Deployment deployment = this.create(Deployment.class);
        Artifact artifact = this.create(Artifact.class);
        Node node = this.create(Node.class);
        deployment.getDeployedArtifacts().add(artifact);
        deployment.setLocation(node);
        Comment comment = this.create(Comment.class);

        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(deployment,
                artifact, comment, null, null);
        assertFalse(canReconnectStatus.isValid());
    }

    /**
     * Test reconnect source with a {@code null} source => reconnection is not
     * authorized.
     */
    @Test
    public void testCanReconnectDeploymentSourceWithNullSource() {
        Deployment deployment = this.create(Deployment.class);
        Artifact artifact = this.create(Artifact.class);
        Node node = this.create(Node.class);
        deployment.getDeployedArtifacts().add(artifact);
        deployment.setLocation(node);

        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(deployment,
                artifact, null, null, null);
        assertFalse(canReconnectStatus.isValid());
    }

    /**
     * Test reconnect {@link Extend} source on a {@link UseCase}, on its target and
     * on a {@link Package}.
     */
    @Test
    public void testCanReconnectExtendSource() {
        // create semantic elements
        Extend extend = this.create(Extend.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);
        extend.setExtension(source);
        extend.setExtendedCase(target);
        UseCase newSource = this.create(UseCase.class);
        Package errorSource = this.create(Package.class);

        // check creation is authorized on an other UseCase
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(extend, source,
                newSource, null, null);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on its target
        canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(extend, source, target,
                null, null);
        assertFalse(canCreateStatus.isValid());

        // can't reconnect on non UseCase
        canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(extend, source, errorSource,
                null, null);
        assertFalse(canCreateStatus.isValid());

        // can't reconnect invalid Extend
        source.getExtends().remove(extend);
        canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(extend, source, newSource,
                null, null);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link Extend} source on a non editable {@link UseCase}
     * source.
     */
    @Test
    public void testCanReconnectExtendSourceOnNoEditableUseCase() {
        // create semantic elements
        Extend extend = this.create(Extend.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);
        extend.setExtension(source);
        extend.setExtendedCase(target);
        UseCase newSource = this.create(UseCase.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(extend, source, newSource, null, null);

        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test can reconnect the {@link Extension} source.
     */
    @Test
    public void testCanReconnectExtensionSource() {
        Stereotype stereotype = this.create(Stereotype.class);
        Stereotype stereotype2 = this.create(Stereotype.class);
        PrimitiveType primitiveType = this.create(PrimitiveType.class);
        Class class1 = this.create(Class.class);
        Extension extension = this.create(Extension.class);

        new ElementDomainBasedEdgeInitializer().initialize(extension, stereotype, class1, null, null, null);
        CheckStatus canReconnect = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(extension,
                stereotype, stereotype2, null, null);
        assertTrue(canReconnect.isValid());
        canReconnect = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(extension, stereotype,
                primitiveType, null, null);
        assertFalse(canReconnect.isValid());
    }

    /**
     * Test reconnect {@link Generalization} source on a {@link Classifier}, on its
     * target and on a {@link Package}.
     */
    @Test
    public void testCanReconnectGeneralizationSource() {
        // create semantic elements
        Generalization generalization = this.create(Generalization.class);
        Actor source = this.create(Actor.class);
        Actor target = this.create(Actor.class);
        generalization.setGeneral(target);
        generalization.setSpecific(source);
        UseCase newSource = this.create(UseCase.class);
        Package errorSource = this.create(Package.class);

        // check creation is authorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(generalization,
                source, newSource, null, null);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on its target
        canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(generalization, source,
                target, null, null);
        assertFalse(canCreateStatus.isValid());

        // can't reconnect on non Classifier
        canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(generalization, source,
                errorSource, null, null);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link Generalization} source on a non editable
     * {@link Classifier} source.
     */
    @Test
    public void testCanReconnectGeneralizationSourceOnNoEditableClassifier() {
        // create semantic elements
        Generalization generalization = this.create(Generalization.class);
        Actor source = this.create(Actor.class);
        UseCase target = this.create(UseCase.class);
        generalization.setGeneral(target);
        generalization.setSpecific(source);
        UseCase newSource = this.create(UseCase.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(generalization, source, newSource, null, null);

        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect source with a non editable {@link Include} .
     */
    @Test
    public void testCanReconnectIncludeSourceNoEditable() {
        // create semantic elements
        Include include = this.create(Include.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);
        include.setAddition(target);
        include.setIncludingCase(source);
        UseCase newSource = this.create(UseCase.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(include, source, newSource, null, null);

        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link Include} with a {@link UseCase}.
     */
    @Test
    public void testCanReconnectIncludeSourceWithUseCase() {
        // create semantic elements
        Include include = this.create(Include.class);
        UseCase source = this.create(UseCase.class);
        UseCase target = this.create(UseCase.class);
        include.setAddition(target);
        include.setIncludingCase(source);
        UseCase newSource = this.create(UseCase.class);
        Actor errorSource = this.create(Actor.class);

        // check reconnection is authorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(include, source,
                newSource, null, null);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on target
        canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(include, source, target,
                null, null);
        assertFalse(canCreateStatus.isValid());

        // can't reconnect on non Use Case
        canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(include, source,
                errorSource, null, null);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Default reconnection for {@link InformationFlow}.
     */
    @Test
    public void testCanReconnectInformationFlowSource() {
        Class source = this.create(Class.class);
        Class newSource = this.create(Class.class);
        Package package1 = this.create(Package.class);
        Comment comment = this.create(Comment.class);
        InformationFlow informationFlow = this.create(InformationFlow.class);
        informationFlow.getInformationSources().add(source);
        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(informationFlow, source, newSource, null, null);
        assertFalse(canCreateStatus.isValid(), "the InformationFlow is not contained in a Package.");
        package1.getPackagedElements().add(informationFlow);
        canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false, new MockedViewQuerier())
                .canReconnect(informationFlow, source, newSource, null, null);
        assertTrue(canCreateStatus.isValid());
        canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false, new MockedViewQuerier())
                .canReconnect(informationFlow, source, comment, null, null);
        assertFalse(canCreateStatus.isValid(), "the InformationFlow new source should be a NamedElement");
    }

    /**
     * Default reconnection for {@link Manifestation}.
     */
    @Test
    public void testCanReconnectManifestationSource() {
        Class source = this.create(Class.class);
        Class newSource = this.create(Class.class);
        Comment comment = this.create(Comment.class);
        Manifestation manifestation = this.create(Manifestation.class);
        manifestation.getClients().add(source);
        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(manifestation, source, newSource, null, null);
        assertTrue(canCreateStatus.isValid());
        canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false, new MockedViewQuerier())
                .canReconnect(manifestation, source, comment, null, null);
        assertFalse(canCreateStatus.isValid(), "the Manifestation new source should be a NamedElement");
    }

    /**
     * Default reconnection for {@link Message}.
     */
    @Test
    public void testCanReconnectMessageSource() {
        Lifeline source = this.create(Lifeline.class);
        Lifeline newSource = this.create(Lifeline.class);
        Comment comment = this.create(Comment.class);
        Message message = this.create(Message.class);
        CheckStatus canReconnectStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(message, source, newSource, null, null);
        assertTrue(canReconnectStatus.isValid());
        canReconnectStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false, new MockedViewQuerier())
                .canReconnect(message, source, comment, null, null);
        assertFalse(canReconnectStatus.isValid(), "the Message new source should be a Lifeline");
    }

    /**
     * Check the canReconnect with ObjectFlow. This test aims to check if the
     * checker mechanism is properly called. This test will not verify all cases
     * since they are already verified by the
     * ElementDomainBasedEdgeCreationCheckerTest.testCanCreateObjectFlowSource test.
     * The
     * org.eclipse.papyrus.uml.domain.services.objectFlow.ObjectFlowHelper.canCreateObjectFlow
     * method is called by both ElementDomainBasedEdgeCreationChecker and
     * ElementDomainBasedEdgeReconnectionSourceChecker.
     */
    @Test
    public void testCanReconnectObjectFlowSource() {
        CreateObjectAction createObjectAction = this.create(CreateObjectAction.class);
        ReclassifyObjectAction reclassifyObjectAction = this.create(ReclassifyObjectAction.class);
        OutputPin outputPin = this.createIn(OutputPin.class, createObjectAction);
        OutputPin newOutputPin = this.createIn(OutputPin.class, createObjectAction);
        InputPin inputPin = this.createIn(InputPin.class, reclassifyObjectAction);
        ObjectFlow objectFlow = this.create(ObjectFlow.class);
        objectFlow.setSource(outputPin);
        objectFlow.setTarget(inputPin);

        // Check an authorize case.
        assertTrue(new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false, new MockedViewQuerier())
                .canReconnect(objectFlow, outputPin, newOutputPin, null, null).isValid());

        // Check a forbidden case.
        FlowFinalNode finalNode = this.create(FlowFinalNode.class);
        assertFalse(new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false, new MockedViewQuerier())
                .canReconnect(objectFlow, outputPin, finalNode, null, null).isValid());
    }

    /**
     * Test reconnect {@link PackageImport} source on a {@link Package} and on
     * {@link Comment}.
     */
    @Test
    public void testCanReconnectPackageImportSource() {
        // create semantic elements
        PackageImport packageImport = this.create(PackageImport.class);
        Package source = this.create(Package.class);
        Package target = this.create(Package.class);
        packageImport.setImportingNamespace(source);
        packageImport.setImportedPackage(target);
        Package newPackageSource = this.create(Package.class);
        UseCase newUseCaseSource = this.create(UseCase.class);
        Comment errorSource = this.create(Comment.class);

        // check reconnection is authorized on Package
        CheckStatus canReconnectStatus = this.elementDomainBasedEdgeReconnectionSourceChecker
                .canReconnect(packageImport, source, newPackageSource, null, null);
        assertTrue(canReconnectStatus.isValid());

        // check reconnection is authorized on UseCase
        canReconnectStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packageImport, source,
                newUseCaseSource, null, null);
        assertTrue(canReconnectStatus.isValid());

        // can reconnect on NameSpace
        canReconnectStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packageImport, source,
                errorSource, null, null);
        assertFalse(canReconnectStatus.isValid());

    }

    /**
     * Test reconnect {@link PackageImport} source on a non editable {@link Package}
     * source.
     */
    @Test
    public void testCanReconnectPackageImportSourceOnNoEditablePackage() {
        // create semantic elements
        PackageImport packageImport = this.create(PackageImport.class);
        Package source = this.create(Package.class);
        Package target = this.create(Package.class);
        packageImport.setImportingNamespace(source);
        packageImport.setImportedPackage(target);
        Package newSource = this.create(Package.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(packageImport, source, newSource, null, null);

        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link PackageMerge} source on a {@link Package} and on an
     * {@link Actor}.
     */
    @Test
    public void testCanReconnectPackageMergeSource() {
        // create semantic elements
        PackageMerge packageMerge = this.create(PackageMerge.class);
        Package source = this.create(Package.class);
        Package target = this.create(Package.class);
        packageMerge.setReceivingPackage(source);
        packageMerge.setMergedPackage(target);
        Package newSource = this.create(Package.class);
        Actor errorSource = this.create(Actor.class);

        // check creation is authorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packageMerge,
                source, newSource, null, null);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on non Package
        canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(packageMerge, source,
                errorSource, null, null);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link PackageMerge} source on a non editable {@link Package}
     * source.
     */
    @Test
    public void testCanReconnectPackageMergeSourceNoEditable() {
        // create semantic elements
        PackageMerge packageMerge = this.create(PackageMerge.class);
        Package source = this.create(Package.class);
        Package target = this.create(Package.class);
        packageMerge.setReceivingPackage(source);
        packageMerge.setMergedPackage(target);
        Package newSource = this.create(Package.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(packageMerge, source, newSource, null, null);

        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link Substitution} source on a {@link Classifier}, on its
     * target and on a {@link Package}.
     */
    @Test
    public void testCanReconnectSubstitutionSource() {
        // create semantic elements
        Substitution substitution = this.create(Substitution.class);
        Actor source = this.create(Actor.class);
        Actor target = this.create(Actor.class);
        substitution.setSubstitutingClassifier(source);
        substitution.setContract(target);
        UseCase newSource = this.create(UseCase.class);
        Package errorSource = this.create(Package.class);

        // check creation is authorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(substitution,
                source, newSource, null, null);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on its target
        canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(substitution, source,
                target, null, null);
        assertFalse(canCreateStatus.isValid());

        // can't reconnect on non Classifier
        canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(substitution, source,
                errorSource, null, null);
        assertFalse(canCreateStatus.isValid());

    }

    /**
     * Test reconnect {@link Substitution} source on a non editable
     * {@link Classifier} source.
     */
    @Test
    public void testCanReconnectSubstitutionSourceOnNoEditableClassifier() {
        // create semantic elements
        Substitution substitution = this.create(Substitution.class);
        Actor source = this.create(Actor.class);
        Actor target = this.create(Actor.class);
        substitution.setContract(source);
        substitution.setSubstitutingClassifier(target);
        UseCase newSource = this.create(UseCase.class);

        CheckStatus canCreateStatus = new ElementDomainBasedEdgeReconnectionSourceChecker(e -> false,
                new MockedViewQuerier()).canReconnect(substitution, source, newSource, null, null);

        assertFalse(canCreateStatus.isValid());
    }

    @Test
    public void testCanReconnectTransitionSource() {
        State state1 = this.create(State.class);
        Pseudostate source1 = this.createIn(Pseudostate.class, state1);
        Region region = this.createIn(Region.class, state1);
        Pseudostate source2 = this.createIn(Pseudostate.class, region);
        Pseudostate target = this.createIn(Pseudostate.class, region);

        Transition transition = this.createIn(Transition.class, region);
        transition.setSource(source1);
        transition.setTarget(target);

        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(transition,
                source1, source2, null, null);
        assertTrue(canCreateStatus.isValid());

        canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(transition, source1, region,
                null, null);
        assertFalse(canCreateStatus.isValid());
    }

    /**
     * Test reconnect {@link Usage} source on a {@link NamedElement}, on its target
     * and on a {@link Comment}.
     */
    @Test
    public void testCanReconnectUsageSource() {
        // create semantic elements
        Usage usage = this.create(Usage.class);
        Actor source = this.create(Actor.class);
        Actor target = this.create(Actor.class);
        usage.getClients().add(source);
        usage.getSuppliers().add(target);
        UseCase newSource = this.create(UseCase.class);
        Comment errorSource = this.create(Comment.class);

        // check creation is authorized
        CheckStatus canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(usage, source,
                newSource, null, null);
        assertTrue(canCreateStatus.isValid());

        // can't reconnect on non NamedElement
        canCreateStatus = this.elementDomainBasedEdgeReconnectionSourceChecker.canReconnect(usage, source, errorSource,
                null, null);
        assertFalse(canCreateStatus.isValid());

    }
}