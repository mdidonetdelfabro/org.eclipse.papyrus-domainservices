/*******************************************************************************
 * Copyright (c) 2023, 2024 CEA LIST.
 * This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *     Obeo - initial API and implementation
 *******************************************************************************/
package org.eclipse.papyrus.uml.domain.services.profile;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.emf.common.util.EMap;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EAnnotation;
import org.eclipse.emf.ecore.EClassifier;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.uml2.uml.Profile;
import org.eclipse.uml2.uml.UMLFactory;
import org.junit.jupiter.api.Test;

/**
 * Test the dynamic profile generation.
 * 
 * @author lfasani
 */
public class ProfileTest {

    /**
     * UML Model containing the definition of a Profile.
     */
    public static final URI PROFILE_UML_MODEL = URI
            .createPlatformPluginURI("/org.eclipse.papyrus.uml.domain.services.test/profile/profile.uml", false);

    private static final String UML_ANNOTATION = "http://www.eclipse.org/uml2/2.0.0/UML";

    @Test
    void testDynamicProfileEcoreGeneration() {
        ResourceSet rs = new ResourceSetImpl();
        Resource profileResource = rs.getResource(PROFILE_UML_MODEL, true);
        EObject eObject = profileResource.getContents().get(0);

        assertTrue(eObject instanceof Profile);
        Profile profile = (Profile) eObject;
        String version1 = "1.0.1";
        String version2 = "2.0.1";
        String comment = "comment";
        String copyright = "copyright";
        String date = "date";
        String author = "author";
        boolean generateEPackageInProfileResult = new DynamicProfileConverter().generateEPackageInProfile(profile, true,
                new ProfileDefinition(ProfileVersion.parseVersion(version1), comment, copyright, date, author));

        assertTrue(generateEPackageInProfileResult, "The ecore package generation failed");

        EAnnotation eAnnotation = profile.getEAnnotation(UML_ANNOTATION);
        assertEquals(1, eAnnotation.getContents().size());
        checkContent(eAnnotation.getContents().get(0), version1, comment, copyright, date, author);

        // generate a second time
        generateEPackageInProfileResult = new DynamicProfileConverter().generateEPackageInProfile(profile, true,
                new ProfileDefinition(ProfileVersion.parseVersion(version2), comment, copyright, date, author));

        eAnnotation = profile.getEAnnotation(UML_ANNOTATION);
        assertEquals(2, eAnnotation.getContents().size());
        checkContent(eAnnotation.getContents().get(0), version2, comment, copyright, date, author);
        checkContent(eAnnotation.getContents().get(1), version1, comment, copyright, date, author);
    }

    /**
     * Check the publication of a profile that contains a sub profile inside. Both
     * profile should be published.
     */
    @Test
    void testDynamicProfileEcoreGenerationWithSubProfile() {
        ResourceSet rs = new ResourceSetImpl();
        Resource profileResource = rs.getResource(PROFILE_UML_MODEL, true);
        EObject eObject = profileResource.getContents().get(0);

        assertTrue(eObject instanceof Profile);
        Profile profile = (Profile) eObject;
        String version = "1.0.1";
        String comment = "comment";
        String copyright = "copyright";
        String date = "date";
        String author = "author";

        Profile subProfile = UMLFactory.eINSTANCE.createProfile();
        subProfile.setName("Sub profile");
        ((Profile) eObject).getPackagedElements().add(subProfile);

        boolean generateEPackageInProfileResult = new DynamicProfileConverter().generateEPackageInProfile(profile, true,
                new ProfileDefinition(ProfileVersion.parseVersion(version), comment, copyright, date, author));

        assertTrue(generateEPackageInProfileResult, "The ecore package generation failed");

        EAnnotation eAnnotation = profile.getEAnnotation(UML_ANNOTATION);
        assertEquals(1, eAnnotation.getContents().size());
        checkContent(eAnnotation.getContents().get(0), version, comment, copyright, date, author);

        EAnnotation eAnnotationSubProfile = subProfile.getEAnnotation(UML_ANNOTATION);
        assertEquals(1, eAnnotationSubProfile.getContents().size());
        checkAnnotationContent(eAnnotationSubProfile.getContents().get(0), version, comment, copyright, date, author);
    }

    private void checkContent(EObject eObject, String version, String comment, String copyright, String date,
            String author) {
        EPackage ePackage = checkAnnotationContent(eObject, version, comment, copyright, date, author);

        EClassifier eClassifier = ePackage.getEClassifier("Stereotype3");
        assertNotNull(eClassifier);
    }

    private EPackage checkAnnotationContent(EObject eObject, String version, String comment, String copyright,
            String date, String author) {
        assertTrue(eObject instanceof EPackage);
        EPackage ePackage = (EPackage) eObject;
        EAnnotation eAnnotation = ePackage.getEAnnotation("PapyrusVersion");
        EMap<String, String> details = eAnnotation.getDetails();
        assertEquals(version, details.get("Version"));
        assertEquals(comment, details.get("Comment"));
        assertEquals(copyright, details.get("Copyright"));
        assertEquals(date, details.get("Date"));
        assertEquals(author, details.get("Author"));
        return ePackage;
    }
}
