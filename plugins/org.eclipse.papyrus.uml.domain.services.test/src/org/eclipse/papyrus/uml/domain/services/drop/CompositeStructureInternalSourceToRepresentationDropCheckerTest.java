/*****************************************************************************
 * Copyright (c) 2022, 2023 CEA LIST, Obeo.
 *
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License 2.0
 * which accompanies this distribution, and is available at
 * https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *
 * Contributors:
 *  Obeo - Initial API and implementation
 *****************************************************************************/
package org.eclipse.papyrus.uml.domain.services.drop;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EcoreFactory;
import org.eclipse.papyrus.uml.domain.services.drop.diagrams.CompositeStructureInternalSourceToRepresentationDropChecker;
import org.eclipse.papyrus.uml.domain.services.status.CheckStatus;
import org.eclipse.papyrus.uml.domain.services.utils.AbstractUMLTest;
import org.eclipse.uml2.uml.Activity;
import org.eclipse.uml2.uml.Class;
import org.eclipse.uml2.uml.Collaboration;
import org.eclipse.uml2.uml.CollaborationUse;
import org.eclipse.uml2.uml.Comment;
import org.eclipse.uml2.uml.Constraint;
import org.eclipse.uml2.uml.Deployment;
import org.eclipse.uml2.uml.FunctionBehavior;
import org.eclipse.uml2.uml.InformationItem;
import org.eclipse.uml2.uml.Interaction;
import org.eclipse.uml2.uml.Namespace;
import org.eclipse.uml2.uml.OpaqueBehavior;
import org.eclipse.uml2.uml.Parameter;
import org.eclipse.uml2.uml.Port;
import org.eclipse.uml2.uml.Property;
import org.eclipse.uml2.uml.StateMachine;
import org.eclipse.uml2.uml.StructuredClassifier;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class CompositeStructureInternalSourceToRepresentationDropCheckerTest extends AbstractUMLTest {

    private CompositeStructureInternalSourceToRepresentationDropChecker compositeStructureInternalSourceToRepresentationDropChecker;

    @Override
    @BeforeEach
    public void setUp() {
        super.setUp();
        this.compositeStructureInternalSourceToRepresentationDropChecker = new CompositeStructureInternalSourceToRepresentationDropChecker();
    }

    /**
     * Test dropping a {@link Activity} on {@link Comment} => not authorized.
     */
    @Test
    public void testActivityDropOnComment() {
        Activity activity = this.create(Activity.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Activity} on {@link Package} => authorized.
     */
    @Test
    public void testActivityDropOnPackage() {
        Activity activity = this.create(Activity.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Activity} on {@link StructuredClassifier} =>
     * authorized.
     */
    @Test
    public void testActivityDropOnStructuredClassifier() {
        Activity activity = this.create(Activity.class);
        Class clazz = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(activity, clazz);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link Class} => authorized.
     */
    @Test
    public void testClassDropOnClass() {
        Class c1 = this.create(Class.class);
        Class c2 = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(c1, c2);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link Comment} => not authorized.
     */
    @Test
    public void testClassDropOnComment() {
        Class clazz = this.create(Class.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(clazz, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Class} on {@link org.eclipse.uml2.uml.Package} =>
     * authorized.
     */
    @Test
    public void testClassDropOnPackage() {
        Class c1 = this.create(Class.class);
        org.eclipse.uml2.uml.Package p1 = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(c1, p1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link Class} => authorized.
     */
    @Test
    public void testCollaborationDropOnClass() {
        Collaboration collab = this.create(Collaboration.class);
        Class c2 = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(collab, c2);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link Comment} => not authorized.
     */
    @Test
    public void testCollaborationDropOnComment() {
        Collaboration collab = this.create(Collaboration.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(collab, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Collaboration} on {@link org.eclipse.uml2.uml.Package}
     * => authorized.
     */
    @Test
    public void testCollaborationDropOnPackage() {
        Collaboration collab = this.create(Collaboration.class);
        org.eclipse.uml2.uml.Package p1 = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(collab, p1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link CollaborationUse} on {@link Comment} => not
     * authorized.
     */
    @Test
    public void testCollaborationUseDropOnComment() {
        CollaborationUse collaborationUse = this.create(CollaborationUse.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(collaborationUse, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link CollaborationUse} on {@link StructuredClassifier} =>
     * authorized.
     */
    @Test
    public void testCollaborationUseDropOnStructuredClassifier() {
        CollaborationUse collaborationUse = this.create(CollaborationUse.class);
        Class clazz = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(collaborationUse, clazz);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on {@link Comment} => not authorized.
     */
    @Test
    public void testCommentDropOnComment() {
        Comment comment1 = this.create(Comment.class);
        Comment comment2 = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment1, comment2);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on non {@link Element} => not authorized.
     */
    @Test
    public void testCommentDropOnEObject() {
        EPackage ePackage = EcoreFactory.eINSTANCE.createEPackage();
        Comment comment1 = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment1, ePackage);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on {@link Element} => authorized.
     */
    @Test
    public void testCommentDropOnPackage() {
        Comment comment1 = this.create(Comment.class);
        org.eclipse.uml2.uml.Package p1 = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment1, p1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on {@link Property} => authorized.
     */
    @Test
    public void testCommentDropOnProperty() {
        Comment comment = this.create(Comment.class);
        Property property = this.create(Property.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment, property);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Comment} on {@link StructuredClassifier} =>
     * authorized.
     */
    @Test
    public void testCommentDropOnStructuredClassifier() {
        Comment comment = this.create(Comment.class);
        Class clazz = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(comment, clazz);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint} on non {@link Namespace} => not
     * authorized.
     */
    @Test
    public void testConstraintDropOnComment() {
        Constraint constraint1 = this.create(Constraint.class);
        Comment comment2 = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraint1, comment2);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint} on {@link Namespace} => authorized.
     */
    @Test
    public void testConstraintDropOnPackage() {
        Constraint constraint1 = this.create(Constraint.class);
        org.eclipse.uml2.uml.Package p1 = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraint1, p1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Constraint} on {@link StructuredClassifier} =>
     * authorized.
     */
    @Test
    public void testConstraintDropOnStructuredClassifier() {
        Constraint constraint = this.create(Constraint.class);
        Class clazz = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(constraint, clazz);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Deployment} on {@link Comment} => not authorized.
     */
    @Test
    public void testDeploymentDropOnComment() {
        Deployment deployment = this.create(Deployment.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(deployment, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link FunctionBehavior} on {@link Class} => authorized.
     */
    @Test
    public void testFunctionBehaviorDropOnClass() {
        FunctionBehavior functionBehavior = this.create(FunctionBehavior.class);
        Class clazz = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(functionBehavior, clazz);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link FunctionBehavior} on {@link Comment} => not
     * authorized.
     */
    @Test
    public void testFunctionBehaviorDropOnComment() {
        FunctionBehavior functionBehavior = this.create(FunctionBehavior.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(functionBehavior, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link FunctionBehavior} on {@link Package} => authorized.
     */
    @Test
    public void testFunctionBehaviorDropOnPackage() {
        FunctionBehavior functionBehavior = this.create(FunctionBehavior.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(functionBehavior, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link InformationItem} on {@link Class} => authorized.
     */
    @Test
    public void testInformationItemDropOnClass() {
        InformationItem informationItem = this.create(InformationItem.class);
        Class c2 = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(informationItem, c2);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link InformationItem} on {@link Comment} => not authorized.
     */
    @Test
    public void testInformationItemDropOnComment() {
        InformationItem informationItem = this.create(InformationItem.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(informationItem, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link InformationItem} on
     * {@link org.eclipse.uml2.uml.Package} => authorized.
     */
    @Test
    public void testInformationItemDropOnPackage() {
        InformationItem informationItem = this.create(InformationItem.class);
        org.eclipse.uml2.uml.Package p1 = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(informationItem, p1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Interaction} on {@link Class} => authorized.
     */
    @Test
    public void testInteractionDropOnClass() {
        Interaction interaction = this.create(Interaction.class);
        Class clazz = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(interaction, clazz);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Interaction} on {@link Comment} => not authorized.
     */
    @Test
    public void testInteractionDropOnComment() {
        Interaction interaction = this.create(Interaction.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(interaction, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Interaction} on {@link Package} => authorized.
     */
    @Test
    public void testInteractionDropOnPackage() {
        Interaction interaction = this.create(Interaction.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(interaction, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link OpaqueBehavior} on {@link Class} => authorized.
     */
    @Test
    public void testOpaqueBehaviorDropOnClass() {
        OpaqueBehavior opaqueBehavior = this.create(OpaqueBehavior.class);
        Class clazz = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(opaqueBehavior, clazz);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link OpaqueBehavior} on {@link Comment} => not authorized.
     */
    @Test
    public void testOpaqueBehaviorDropOnComment() {
        OpaqueBehavior opaqueBehavior = this.create(OpaqueBehavior.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(opaqueBehavior, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link OpaqueBehavior} on {@link Package} => authorized.
     */
    @Test
    public void testOpaqueBehaviorDropOnPackage() {
        OpaqueBehavior opaqueBehavior = this.create(OpaqueBehavior.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(opaqueBehavior, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Parameter} on {@link Class} => not authorized.
     */
    @Test
    public void testParameterDropOnClass() {
        Parameter parameter = this.create(Parameter.class);
        Class clazz = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(parameter, clazz);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Parameter} on {@link Collaboration} => not authorized.
     */
    @Test
    public void testParameterDropOnCollaboration() {
        Parameter parameter = this.create(Parameter.class);
        Collaboration collaboration = this.create(Collaboration.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(parameter, collaboration);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Parameter} on {@link Comment} => not authorized.
     */
    @Test
    public void testParameterDropOnComment() {
        Parameter parameter = this.create(Parameter.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(parameter, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link OpaqueBehavior} on {@link StructuredClassifier} =>
     * authorized.
     */
    @Test
    public void testParameterDropOnStructuredClassifier() {
        Parameter opaqueBehavior = this.create(Parameter.class);
        Activity activity = this.create(Activity.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(opaqueBehavior, activity);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on {@link StructuredClassifier} => authorized.
     */
    @Test
    public void testPortDropOnClass() {
        Port port = this.create(Port.class);
        Class c1 = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(port, c1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on typed {@link Port} => authorized.
     */
    @Test
    public void testPortDropOnTypedPort() {
        Port port1 = this.create(Port.class);
        Port port2 = this.create(Port.class);
        Class c1 = this.create(Class.class);
        port2.setType(c1);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(port1, port2);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Port} on typed {@link Property} => authorized.
     */
    @Test
    public void testPortDropOnTypedProperty() {
        Port port1 = this.create(Port.class);
        Property p2 = this.create(Property.class);
        Class c1 = this.create(Class.class);
        p2.setType(c1);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(port1, p2);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link StructuredClassifier} =>
     * authorized.
     */
    @Test
    public void testPropertyDropOnClass() {
        Property p1 = this.create(Property.class);
        Class c1 = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(p1, c1);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Comment} => not authorized.
     */
    @Test
    public void testPropertyDropOnComment() {
        Property p1 = this.create(Property.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(p1, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Port} => not authorized.
     */
    @Test
    public void testPropertyDropOnPort() {
        Property p1 = this.create(Property.class);
        Port port = this.create(Port.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(p1, port);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on {@link Property} => not authorized.
     */
    @Test
    public void testPropertyDropOnProperty() {
        Property p1 = this.create(Property.class);
        Property p2 = this.create(Property.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(p1, p2);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link Property} on typed {@link Property} => authorized.
     */
    @Test
    public void testPropertyDropOnTypedProperty() {
        Property p1 = this.create(Property.class);
        Property p2 = this.create(Property.class);
        Class c1 = this.create(Class.class);
        p2.setType(c1);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(p1, p2);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link StateMachine} on {@link Class} => authorized.
     */
    @Test
    public void testStateMachineDropOnClass() {
        StateMachine stateMachine = this.create(StateMachine.class);
        Class clazz = this.create(Class.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(stateMachine, clazz);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link StateMachine} on {@link Comment} => not authorized.
     */
    @Test
    public void testStateMachineDropOnComment() {
        StateMachine stateMachine = this.create(StateMachine.class);
        Comment comment = this.create(Comment.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(stateMachine, comment);
        assertFalse(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link StateMachine} on {@link Package} => authorized.
     */
    @Test
    public void testStateMachineDropOnPackage() {
        StateMachine stateMachine = this.create(StateMachine.class);
        org.eclipse.uml2.uml.Package pack = this.create(org.eclipse.uml2.uml.Package.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(stateMachine, pack);
        assertTrue(canDragAndDropStatus.isValid());
    }

    /**
     * Test dropping a {@link StateMachine} on {@link Property} => authorized.
     */
    @Test
    public void testStateMachineDropOnProperty() {
        StateMachine stateMachine = this.create(StateMachine.class);
        Property property = this.create(Property.class);

        CheckStatus canDragAndDropStatus = this.compositeStructureInternalSourceToRepresentationDropChecker
                .canDragAndDrop(stateMachine, property);
        assertFalse(canDragAndDropStatus.isValid());
    }

}
